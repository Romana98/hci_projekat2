package com.example.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "maps")
public class Maps {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "map_id", unique=true, nullable = false)
	private Integer id;
	
	
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="map")
	private Set<Architect> architects = new HashSet<Architect>();
	
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="map")
	private Set<ArchitecturalWork> architecturalWorks = new HashSet<ArchitecturalWork>();
	
	public Maps() {
		super();
	}
	
	

	public Maps(Integer id) {
		super();
		this.id = id;
	}



	public Maps(Integer id, String name, Set<Architect> architects, Set<ArchitecturalWork> architecturalWorks) {
		super();
		this.id = id;
		this.architects = architects;
		this.architecturalWorks = architecturalWorks;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Set<Architect> getArchitects() {
		return architects;
	}

	public void setArchitects(Set<Architect> architects) {
		this.architects = architects;
	}

	public Set<ArchitecturalWork> getArchitecturalWorks() {
		return architecturalWorks;
	}

	public void setArchitecturalWorks(Set<ArchitecturalWork> architecturalWorks) {
		this.architecturalWorks = architecturalWorks;
	}
	

}
