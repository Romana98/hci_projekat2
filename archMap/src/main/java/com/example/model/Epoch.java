package com.example.model;

public enum Epoch {
	

	PREHISTORIC_ARCHITECTURE,
	ANCIENT_MESOPOTAMIA, 
	ANTIQUE, 
	BYZANTIUM,
	EARLY_CHRISTIANITY,
	ROMANESQUE,
	GOTHIC,
	RENAISSANCE,
	BAROQUE,
	CLASSICISM,
	MODERNISM,
	POSTMODERNISM;
	
	/*@Override
	  public String toString() {
	    switch(this) {
	      case PREHISTORIC_ARCHITECTURE: return "Prehistoric architecture";
	      case ANCIENT_MESOPOTAMIA: return "Ancient Mesopotamia";
	      case ANTIQUE: return "Ancient Mesopotamia";
	      case BYZANTIUM: return "Ancient Mesopotamia";
	      case EARLY_CHRISTIANITY: return "Ancient Mesopotamia";
	      case ROMANESQUE: return "Ancient Mesopotamia";
	      case GOTHIC: return "Ancient Mesopotamia";
	      case RENAISSANCE: return "Ancient Mesopotamia";
	      case BAROQUE: return "Ancient Mesopotamia";
	      case CLASSICISM: return "Ancient Mesopotamia";
	      case MODERNISM: return "Ancient Mesopotamia";
	      case POSTMODERNISM: return "Ancient Mesopotamia";
	      default: throw new IllegalArgumentException();
	    }
	  }*/
}
