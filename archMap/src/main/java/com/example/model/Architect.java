package com.example.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "architects", uniqueConstraints = {@UniqueConstraint(columnNames = {"latitude", "longitude"})})
public class Architect {
	
	@Id
	@Column(name = "arch_id", unique = true, nullable = false)
	private String id;

	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "surname", nullable = false)
	private String surname;
	
	@Column(name = "birth", nullable = false)
	private Date dateBorn;
	
	@Column(name = "death", nullable = true)
	private Date dateDeath;
	
	@Column(name = "longitude", nullable = false)
	private Double longitude;
	
	@Column(name = "latitude", nullable = false)
	private Double latitude;
	
	@Column(name = "nationality", nullable = false)
	private String nationality;
	
	@Column(name = "photo", nullable = false, length=10485760)
	private String photo;
	
	@OneToMany(cascade={CascadeType.ALL}, fetch=FetchType.LAZY, mappedBy="architect")
	private Set<ArchitecturalWork> architecturalWorks = new HashSet<ArchitecturalWork>();
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="map", referencedColumnName="map_id")
	private Maps map;
	
	public Architect() {
		super();
	}

	public Architect(String id, String name, String surname, Date dateBorn, Date dateDeath, Double longitude, Double latitude,
			String nationality, String photo, Set<ArchitecturalWork> architecturalWorks, Maps map) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.dateBorn = dateBorn;
		this.dateDeath = dateDeath;
		this.longitude = longitude;
		this.latitude = latitude;
		this.nationality = nationality;
		this.photo = photo;
		this.architecturalWorks = architecturalWorks;
		this.map = map;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getDateBorn() {
		return dateBorn;
	}

	public void setDateBorn(Date dateBorn) {
		this.dateBorn = dateBorn;
	}

	public Date getDateDeath() {
		return dateDeath;
	}

	public void setDateDeath(Date dateDeath) {
		this.dateDeath = dateDeath;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Set<ArchitecturalWork> getArchitecturalWorks() {
		return architecturalWorks;
	}

	public void setArchitecturalWorks(HashSet<ArchitecturalWork> architecturalWorks) {
		this.architecturalWorks = architecturalWorks;
	}

	public Maps getMap() {
		return this.map;
	}

	public void setMap(Maps map) {
		this.map = map;
	}

	public void setArchitecturalWorks(Set<ArchitecturalWork> architecturalWorks) {
		this.architecturalWorks = architecturalWorks;
	}
	
	

}
