package com.example.model;

import java.util.Date;
import javax.persistence.*;


@Entity
@Table(name = "architectural_works", uniqueConstraints = {@UniqueConstraint(columnNames = {"latitude", "longitude"})})
public class ArchitecturalWork {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "work_id", unique=true, nullable = false)
	private Integer id;
	
	@Column(name = "name", nullable = false, unique = true)
	private String name;
	
	@Column(name = "latitude", nullable = false)
	private Double latitude;
	
	@Column(name = "longitude", nullable = false)
	private Double longitude;
	
	@Column(name = "description", nullable = false, length=10485760)
	private String description;
	
	@Column(name = "year_built", nullable = false)
	private Date yearBuilt;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="architect", referencedColumnName="arch_id", nullable=false)
	private Architect architect;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="map", referencedColumnName="map_id", nullable = false)
	private Maps map;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "epoch", nullable = false)
	private Epoch epoch;
	
	@Column(name = "world_wonder", nullable = false)
	private boolean worldWonder;
	
	@Column(name = "protected_work", nullable = false)
	private boolean protectedWork;
	
	@Column(name = "purpose", nullable = false)
	private String purpose;
	
	@Column(name = "icon", nullable = false, length=10485760)
	private String icon;
	
	public ArchitecturalWork() {
		super();
	}

	public ArchitecturalWork(String name, String description, Date yearBuilt, Architect architect,
			Epoch epoch, boolean worldWonder, boolean protectedWork, String purpose, String icon) {
		super();
		this.name = name;
		this.description = description;
		this.yearBuilt = yearBuilt;
		this.architect = architect;
		this.epoch = epoch;
		this.worldWonder = worldWonder;
		this.protectedWork = protectedWork;
		this.purpose = purpose;
		this.icon = icon;
	}
	
	public ArchitecturalWork(Integer id, String name, Double latitude, Double longitude, String description,
			Date yearBuilt, Architect architect, Epoch epoch, boolean worldWonder, boolean protectedWork,
			String purpose, String icon) {
		super();
		this.id = id;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.description = description;
		this.yearBuilt = yearBuilt;
		this.architect = architect;
		this.epoch = epoch;
		this.worldWonder = worldWonder;
		this.protectedWork = protectedWork;
		this.purpose = purpose;
		this.icon = icon;
	}
	
	public ArchitecturalWork(String name, Double latitude, Double longitude, String description,
			Date yearBuilt, Architect architect, Epoch epoch, boolean worldWonder, boolean protectedWork,
			String purpose, String icon) {
		super();
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.description = description;
		this.yearBuilt = yearBuilt;
		this.architect = architect;
		this.epoch = epoch;
		this.worldWonder = worldWonder;
		this.protectedWork = protectedWork;
		this.purpose = purpose;
		this.icon = icon;
	}
	
	
	public ArchitecturalWork(Integer id, String name, Double latitude, Double longitude, String description,
			Date yearBuilt, Architect architect, Epoch epoch, boolean worldWonder, boolean protectedWork,
			String purpose, String icon, Maps map) {
		super();
		this.id = id;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.description = description;
		this.yearBuilt = yearBuilt;
		this.architect = architect;
		this.map = map;
		this.epoch = epoch;
		this.worldWonder = worldWonder;
		this.protectedWork = protectedWork;
		this.purpose = purpose;
		this.icon = icon;
	}
	

	public ArchitecturalWork(String name, Double latitude, Double longitude, String description, Date yearBuilt,
			Architect architect,  Epoch epoch, boolean worldWonder, boolean protectedWork, String purpose,
			String icon, Maps map) {
		super();
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.description = description;
		this.yearBuilt = yearBuilt;
		this.architect = architect;
		this.map = map;
		this.epoch = epoch;
		this.worldWonder = worldWonder;
		this.protectedWork = protectedWork;
		this.purpose = purpose;
		this.icon = icon;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getYearBuilt() {
		return yearBuilt;
	}

	public void setYearBuilt(Date yearBuilt) {
		this.yearBuilt = yearBuilt;
	}

	public Architect getArchitect() {
		return architect;
	}

	public void setArchitect(Architect architect) {
		this.architect = architect;
	}

	public Epoch getEpoch() {
		return epoch;
	}

	public void setEpoch(Epoch epoch) {
		this.epoch = epoch;
	}

	public boolean isWorldWonder() {
		return worldWonder;
	}

	public void setWorldWonder(boolean worldWonder) {
		this.worldWonder = worldWonder;
	}

	public boolean isProtectedWork() {
		return protectedWork;
	}

	public void setProtectedWork(boolean protectedWork) {
		this.protectedWork = protectedWork;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Maps getMap() {
		return this.map;
	}

	public void setMap(Maps map) {
		this.map = map;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	
	
	
}
