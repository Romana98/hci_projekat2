package com.example.dto;

public class ParamDTO {
	
	private boolean active;
	private String param;
	private String value1;
	private String value2;
	public ParamDTO() {
		super();
	}
	public ParamDTO(boolean active, String param, String value1, String value2) {
		super();
		this.active = active;
		this.param = param;
		this.value1 = value1;
		this.value2 = value2;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public String getValue1() {
		return value1;
	}
	public void setValue1(String value1) {
		this.value1 = value1;
	}
	public String getValue2() {
		return value2;
	}
	public void setValue2(String value2) {
		this.value2 = value2;
	}
}