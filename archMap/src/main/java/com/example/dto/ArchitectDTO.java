package com.example.dto;

import java.util.ArrayList;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ArchitectDTO {
	
	@NotNull
	@NotBlank
	private String id;

	@NotNull
	@NotBlank
	private String name;
	
	@NotNull
	@NotBlank
	private String surname;
	
	@NotNull
	private Date dateBorn;
	
	private Date dateDeath;
	
	private Double latitude;
	private Double longitude;
	
	@NotNull
	@NotBlank
	private String nationality;
	
	private String photo;
	
	private int map_id;
	
	private boolean searchResult;
	
	private String style;
	
	private ArrayList<ArchitecturalWorkDTO> works;

	public ArchitectDTO() {
		super();
		this.works = new ArrayList<ArchitecturalWorkDTO>();
		this.setSearchResult(false);
	}


	public ArchitectDTO(@NotNull @NotBlank String id, @NotNull @NotBlank String name, @NotNull @NotBlank String surname,
			@NotNull Date dateBorn, Date dateDeath, Double latitude, Double longitude, @NotNull @NotBlank String nationality,
			String photo) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.dateBorn = dateBorn;
		this.dateDeath = dateDeath;
		this.latitude = latitude;
		this.longitude = longitude;
		this.nationality = nationality;
		this.photo = photo;
		this.works = new ArrayList<ArchitecturalWorkDTO>();
		this.setSearchResult(false);
	}
	
	
	
	public String getStyle() {
		return style;
	}


	public void setStyle() {
		if(this.searchResult == true) {
			this.style = "background: aqua;margin-left:7px; margin-top:7px;  width: 46px; height: 46px";
		}else {
			this.style = "margin-left:0px; margin-top:0px;  width: 60px; height: 60px";
		}
	}


	public boolean isSearchResult() {
		return searchResult;
	}


	public void setSearchResult(boolean searchResult) {
		this.searchResult = searchResult;
		this.setStyle();
	}


	public void addWork(ArchitecturalWorkDTO awdto) {
		this.works.add(awdto);
	}
	
	public ArrayList<ArchitecturalWorkDTO> getWorks() {
		return works;
	}


	public void setWorks(ArrayList<ArchitecturalWorkDTO> works) {
		this.works = works;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getDateBorn() {
		return dateBorn;
	}

	public void setDateBorn(Date dateBorn) {
		this.dateBorn = dateBorn;
	}

	public Date getDateDeath() {
		return dateDeath;
	}

	public void setDateDeath(Date dateDeath) {
		this.dateDeath = dateDeath;
	}

	public Double getLatitude() {
		return latitude;
	}


	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}


	public Double getLongitude() {
		return longitude;
	}


	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}


	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public int getMap_id() {
		return map_id;
	}


	public void setMap_id(int map_id) {
		this.map_id = map_id;
	}
}
