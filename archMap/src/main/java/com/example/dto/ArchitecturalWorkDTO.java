package com.example.dto;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class ArchitecturalWorkDTO {
	
	private Integer id;
	
	@NotNull
	@NotBlank
	private String name;
	
	private Double latitude;
	
	private Double longitude;
	
	@NotNull
	@NotBlank
	private String description;
	
	@NotNull
	private Date yearBuilt;
	
	@NotNull
	@NotBlank
	private String architectId;
	
	@NotNull
	private String epoch;
	
	@NotNull
	private boolean worldWonder;
	
	@NotNull
	private boolean protectedWork;
	
	@NotNull
	@NotBlank
	private String purpose;
	
	private String icon;
	
	private Integer mapId;

	private boolean searchResult;
	
	private String style;
	
	public ArchitecturalWorkDTO() {
		super();
		this.setSearchResult(false);

	}
	
	
	

	public ArchitecturalWorkDTO(Integer id, String name, Double latitude, Double longitude,
			String description, Date yearBuilt, String architectId, String epoch, boolean worldWonder, boolean protectedWork,
			String purpose, String icon) {
		super();
		this.id = id;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.description = description;
		this.yearBuilt = yearBuilt;
		this.architectId = architectId;
		this.epoch = epoch;
		this.worldWonder = worldWonder;
		this.protectedWork = protectedWork;
		this.purpose = purpose;
		this.icon = icon;
		this.setSearchResult(false);

	}
	
	public ArchitecturalWorkDTO(Integer id, String name, Double latitude, Double longitude,
			String description, Date yearBuilt, String architectId, String epoch, boolean worldWonder, boolean protectedWork,
			String purpose, String icon, Integer mapId) {
		super();
		this.id = id;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		this.description = description;
		this.yearBuilt = yearBuilt;
		this.architectId = architectId;
		this.epoch = epoch;
		this.worldWonder = worldWonder;
		this.protectedWork = protectedWork;
		this.purpose = purpose;
		this.icon = icon;
		this.mapId = mapId;
		this.setSearchResult(false);

	}




	public ArchitecturalWorkDTO(Integer id, String name,
			String description, Date yearBuilt, String architectId, String epoch, boolean worldWonder, boolean protectedWork,
			String purpose, String icon) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.yearBuilt = yearBuilt;
		this.architectId = architectId;
		this.epoch = epoch;
		this.worldWonder = worldWonder;
		this.protectedWork = protectedWork;
		this.purpose = purpose;
		this.icon = icon;
		this.setSearchResult(false);
	}

	public String getStyle() {
		return style;
	}


	public void setStyle() {
		if(this.searchResult == true) {
			this.style = "background: aqua;margin-left:7px; margin-top:7px;  width: 46px; height: 46px";
		}else {
			this.style = "margin-left:0px; margin-top:0px;  width: 60px; height: 60px";
		}
	}


	public boolean isSearchResult() {
		return searchResult;
		
	}


	public void setSearchResult(boolean searchResult) {
		this.searchResult = searchResult;
		this.setStyle();

	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	public Double getLatitude() {
		return latitude;
	}



	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}



	public Double getLongitude() {
		return longitude;
	}



	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}



	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getYearBuilt() {
		return yearBuilt;
	}

	public void setYearBuilt(Date yearBuilt) {
		this.yearBuilt = yearBuilt;
	}

	public String getArchitectId() {
		return architectId;
	}

	public void setArchitectId(String architectId) {
		this.architectId = architectId;
	}

	public String getEpoch() {
		return epoch;
	}

	public void setEpoch(String epoch) {
		this.epoch = epoch;
	}

	public boolean isWorldWonder() {
		return worldWonder;
	}

	public void setWorldWonder(boolean worldWonder) {
		this.worldWonder = worldWonder;
	}

	public boolean isProtectedWork() {
		return protectedWork;
	}

	public void setProtectedWork(boolean protectedWork) {
		this.protectedWork = protectedWork;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}




	public Integer getMapId() {
		return mapId;
	}




	public void setMapId(Integer mapId) {
		this.mapId = mapId;
	}


	
	
}
