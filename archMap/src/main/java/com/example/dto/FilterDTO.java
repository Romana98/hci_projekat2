package com.example.dto;

import java.util.ArrayList;

public class FilterDTO {
	
	private ArrayList<ArchitectDTO> architects;
	private ArrayList<ArchitecturalWorkDTO> works;
	private ArrayList<ParamDTO> params;
	
	public FilterDTO(ArrayList<ArchitectDTO> architects, ArrayList<ArchitecturalWorkDTO> works,
			ArrayList<ParamDTO> params) {
		super();
		this.architects = architects;
		this.works = works;
		this.params = params;
	}

	public FilterDTO() {
		super();
	}

	public ArrayList<ArchitectDTO> getArchitects() {
		return architects;
	}

	public void setArchitects(ArrayList<ArchitectDTO> architects) {
		this.architects = architects;
	}

	public ArrayList<ArchitecturalWorkDTO> getWorks() {
		return works;
	}

	public void setWorks(ArrayList<ArchitecturalWorkDTO> works) {
		this.works = works;
	}

	public ArrayList<ParamDTO> getParams() {
		return params;
	}

	public void setParams(ArrayList<ParamDTO> params) {
		this.params = params;
	}
	
}