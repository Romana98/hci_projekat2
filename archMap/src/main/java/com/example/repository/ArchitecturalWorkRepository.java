package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.ArchitecturalWork;

public interface ArchitecturalWorkRepository extends JpaRepository<ArchitecturalWork, Integer>{
	ArchitecturalWork findByName(String name);
}
