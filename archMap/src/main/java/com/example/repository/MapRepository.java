package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Maps;


public interface MapRepository extends JpaRepository<Maps, Integer> {

}
