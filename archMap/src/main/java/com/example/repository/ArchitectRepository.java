package com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.model.Architect;

public interface ArchitectRepository extends JpaRepository<Architect, String>  {
		
}
