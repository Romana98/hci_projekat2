package com.example.archMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;



@SpringBootApplication
@EntityScan(basePackages = {"com.example.model"}) 
@ComponentScan("com") // zamenjuje onaj basePackages za sve pakete
@EnableJpaRepositories("com.example.repository")
public class ArchMapApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(ArchMapApplication.class, args);

		
	}
}