package com.example.api;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.ArchitecturalWorkDTO;
import com.example.model.Architect;
import com.example.model.ArchitecturalWork;
import com.example.model.Epoch;
import com.example.model.Maps;
import com.example.service.ArchitectService;
import com.example.service.ArchitecturalWorkService;
import com.example.service.MapService;
import com.example.service.initializeAndUnproxy;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/architectural_works")
@RestController
public class ArchitecturalWorkController<T> {
	
	private final ArchitecturalWorkService aws;
	private final ArchitectService as;
	private final MapService ms;
	
	@Autowired
	public ArchitecturalWorkController(ArchitecturalWorkService aws, ArchitectService as, MapService ms) {
		this.aws = aws;
		this.as = as;
		this.ms = ms;
	}
	
	@PostMapping("/addArchWork")
	public ResponseEntity<Integer> addArchWork(@Valid @RequestBody ArchitecturalWorkDTO aw){
		Architect ar = as.getArchitect(aw.getArchitectId());
		com.example.model.Maps map = ms.getMap(aw.getMapId());
		
		if(map == null)
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
			
		if(ar == null)
			return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
		
		
		int flag = aws.save(new ArchitecturalWork(aw.getName(), aw.getLatitude(), aw.getLongitude(),
				aw.getDescription(), aw.getYearBuilt(),ar, Epoch.valueOf(aw.getEpoch()),
				 aw.isWorldWonder(), aw.isProtectedWork(), aw.getPurpose(), aw.getIcon(), map));
		
		if(flag == 0)
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		else
			return ResponseEntity.status(HttpStatus.OK).body(flag);
	}
	
	@PostMapping("/editArchWork")
	public ResponseEntity<T> editArchWork(@Valid @RequestBody ArchitecturalWorkDTO aw){
		Architect ar = as.getArchitect(aw.getArchitectId());
		
		if(ar == null)
			return ResponseEntity.status(HttpStatus.CONFLICT).body(null);

		int flag = aws.edit(new ArchitecturalWork(aw.getId(),aw.getName(), aw.getLatitude(), aw.getLongitude(), 
				aw.getDescription(), aw.getYearBuilt(), ar, Epoch.valueOf(aw.getEpoch()),
				 aw.isWorldWonder(), aw.isProtectedWork(), aw.getPurpose(), aw.getIcon(), (com.example.model.Maps) new Maps(aw.getMapId())));
		
		if(flag == 0)
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		else
			return ResponseEntity.status(HttpStatus.OK).body(null);
	}
	
	@GetMapping("/searchArchWork")
	public ResponseEntity<List<ArchitecturalWorkDTO>> searchArchWork(@RequestParam String parameter, String value){	
		
		if(value.equals("") || parameter.equals(""))
			return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
		
		List<ArchitecturalWorkDTO> awsl = aws.search(parameter, value);
		
		if (awsl.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		else {
			return ResponseEntity.status(HttpStatus.OK).body(awsl);
		}
	}
	
	@DeleteMapping("/deleteArchWork")
	public ResponseEntity<T> deleteArchWork(@RequestParam int work_id) {
		int flag = aws.delete(work_id);
		
		if(flag == 0) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		else {
			return ResponseEntity.status(HttpStatus.OK).body(null);
		}
	}
	
	
	@GetMapping("/getArchWorks")
	public List<ArchitecturalWorkDTO> getArchWorks(){
		List<ArchitecturalWorkDTO>awDTO = new ArrayList<ArchitecturalWorkDTO>();
		List<ArchitecturalWork> awsl = aws.getArchWorks();
		for (ArchitecturalWork aw : awsl) {
			aw.setMap(initializeAndUnproxy.initAndUnproxy(aw.getMap()));
			awDTO.add(new ArchitecturalWorkDTO(aw.getId(), aw.getName(), aw.getLatitude(), aw.getLongitude(), aw.getDescription(), new Date(aw.getYearBuilt().getTime()),aw.getArchitect().getId(), aw.getEpoch().toString(), aw.isWorldWonder(), aw.isProtectedWork(), aw.getPurpose(), aw.getIcon(), aw.getMap().getId()));
		}
		return awDTO;
	}
	
	@GetMapping("/getArchWorksMap")
	public List<ArchitecturalWorkDTO> getArchWorks(@RequestParam int map_id){
		List<ArchitecturalWorkDTO> awsl = aws.getArchWorks(map_id);
		
		return awsl;
	}
	
	@GetMapping("/getEpochs")
	public ResponseEntity<Object> getEpoch(){
		List<String> aws = this.aws.getEpochs();
		if(aws.isEmpty()) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
		}
		else {
			return ResponseEntity.status(HttpStatus.OK).body(aws);
		}
		
	}
	
	@GetMapping("/getPurposes")
	public ResponseEntity<Object> getPurpose(){
		List<String> aws = this.aws.getPurposes();
		if(aws.isEmpty()) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
		}
		else {
			return ResponseEntity.status(HttpStatus.OK).body(aws);
		}
		
	}

}
