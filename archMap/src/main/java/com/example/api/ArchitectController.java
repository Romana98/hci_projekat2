package com.example.api;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.ArchitectDTO;
import com.example.model.Architect;
import com.example.model.ArchitecturalWork;
import com.example.service.ArchitectService;
import com.example.service.ArchitecturalWorkService;

@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/architects")
@RestController
public class ArchitectController<T> {
	
	private final ArchitectService as;
	private final ArchitecturalWorkService aws;
	
	@Autowired
	public ArchitectController(ArchitectService as, ArchitecturalWorkService aws) {
		this.as = as;
		this.aws = aws;
	}
	
	@PostMapping("/addArchitect")
	public ResponseEntity<T> addArchWork(@Valid @RequestBody ArchitectDTO adto){
		
		int flag = as.addArchitect(adto);
		
		if(flag == 0)
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		else
			return ResponseEntity.status(HttpStatus.OK).body(null);
	}
	
	@GetMapping("/getArchitectsMap")
	public List<ArchitectDTO> getArchitects(@RequestParam int map_id){
		List<ArchitectDTO> dtos = new ArrayList<ArchitectDTO>();
		
		dtos = as.getAllArchitectsDTO(map_id);
		
		return dtos;
	}
	
	@GetMapping("/getAllArchitects")
	public List<ArchitectDTO> getAllArchitects(@RequestParam int map_id){
		List<ArchitectDTO> dtos = new ArrayList<ArchitectDTO>();
		
		dtos = as.getAllArchitectsDTO();
		
		return dtos;
	}
	
	@GetMapping("/getArchitects")
	public List<ArchitectDTO> getArchitects(){
		List<ArchitectDTO> dtos = new ArrayList<ArchitectDTO>();
		
		dtos = as.getAllArchitectsDTO();
		
		return dtos;
	}
	
	@PostMapping("/editArchitect")
	public ResponseEntity<T> editArchitect(@Valid @RequestBody ArchitectDTO adto){
		
		int flag = as.editArchitect(adto);
		
		if(flag == 0) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
		}else {
			return ResponseEntity.status(HttpStatus.OK).body(null);
		}
	}
	
	@GetMapping("/searchArchitect")
	public ResponseEntity<ArrayList<ArchitectDTO>> searchArchitect(@RequestParam String parameter, String value){
		
		ArrayList<ArchitectDTO> dtos = new ArrayList<ArchitectDTO>();
		if(parameter.equals("")) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}else {
			if(value.equals("")) {
				dtos = as.getAllArchitectsDTO();
				return ResponseEntity.status(HttpStatus.OK).body(dtos);
			}else {
				dtos = as.searchArchitect(parameter, value);
				if(dtos == null) {
					return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
				}else {
					if(dtos.size() == 0) {
						return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(null);
					}
					return ResponseEntity.status(HttpStatus.OK).body(dtos);
				}
			}
		}
	}
	
	@GetMapping("/getArchitectsAndWorks")
	public ResponseEntity<ArrayList<ArchitectDTO>> getAll(){
		
		ArrayList<ArchitectDTO> architects = as.getAllArchitectsDTO();
		
		
		return ResponseEntity.status(HttpStatus.OK).body(architects);
	}
	
	@DeleteMapping("/deleteArchitect")
	public ResponseEntity<T> deleteArchitect(@RequestParam String arch_id){
		Architect found = as.getArchitect(arch_id);
		for(ArchitecturalWork aw : found.getArchitecturalWorks()) {
			int flag = aws.delete(aw.getId());
			if(flag == 0) {
				return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(null);
			}
		}
		int flag = as.delete(arch_id);
		if(flag == 0) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
		}else {
			return ResponseEntity.status(HttpStatus.OK).body(null);
		}
		
		
	}
	
	@GetMapping("/getNationalities")
	public ResponseEntity<Object> getNationalites(){
		List<String> nationalities = as.getNationalities();
		if(nationalities.isEmpty()) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
		}
		else {
			return ResponseEntity.status(HttpStatus.OK).body(nationalities);
		}
		
	}
	
	

}
