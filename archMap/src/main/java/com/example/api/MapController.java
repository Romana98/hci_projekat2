package com.example.api;
 
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.ArchitectDTO;
import com.example.dto.ArchitecturalWorkDTO;
import com.example.dto.FilterDTO;
import com.example.dto.MapDTO;
import com.example.service.MapService;
 
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/maps")
@RestController
public class MapController<T> {
   
private final MapService ms;
   
    @Autowired
    public MapController(MapService ms) {
        this.ms = ms;
    }
   
    @PostMapping("/saveMap")
    public ResponseEntity<ArrayList<T>> saveMap(@RequestBody MapDTO map){
        int flag = ms.saveMap(map);
       
        if(flag == 0) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }else {
            return ResponseEntity.status(HttpStatus.OK).body(null);
        }
    }
   
    @GetMapping("/searchArchitectsTable")
    public ResponseEntity<ArrayList<ArchitectDTO>> searchArchitectsTable(@RequestParam String param){
        ArrayList<ArchitectDTO> result = ms.searchArchitects(param, "t");
        if(result.size() == 0) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }else {
            return ResponseEntity.status(HttpStatus.OK).body(result);
        }
    }
    
    @PostMapping("/filterArchitectsTable")
	public ResponseEntity<ArrayList<ArchitectDTO>> filterArchitectsTable(@RequestBody FilterDTO filters){
		ArrayList<ArchitectDTO> result = ms.filterArchitectsTable(filters);
		return ResponseEntity.status(HttpStatus.OK).body(result);
	}
   
    @GetMapping("/searchArchitectsMap")
    public ResponseEntity<ArrayList<ArchitectDTO>> searchArchitectsMap(@RequestParam String param){
        return ResponseEntity.status(HttpStatus.OK).body(ms.searchArchitects(param, "m"));
    }
   
    @GetMapping("/searchArchWorksMap")
    public ResponseEntity<ArrayList<ArchitecturalWorkDTO>> searchArchWorksMap(@RequestParam String param){
        return ResponseEntity.status(HttpStatus.OK).body(ms.searchArchitecturalWorks(param));
    }
}