package com.example.service;
 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dto.ArchitectDTO;
import com.example.dto.ArchitecturalWorkDTO;
import com.example.dto.FilterDTO;
import com.example.dto.MapDTO;
import com.example.dto.ParamDTO;
import com.example.model.Architect;
import com.example.model.ArchitecturalWork;
import com.example.model.Maps;
import com.example.repository.ArchitectRepository;
import com.example.repository.ArchitecturalWorkRepository;
import com.example.repository.MapRepository;
 
@Service
public class MapService {
 
    @Autowired
    private MapRepository mr;
   
    @Autowired
    private ArchitectRepository ar;
   
    @Autowired
    private ArchitecturalWorkRepository awr;
 
    public List<Maps> getMaps() {
        return mr.findAll();
    }
 
    public int saveMap(MapDTO map) {
       
        int flag = 0;
        try {
               
                if(map.getIds().startsWith("a"))
                {
                    Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "");
                   
                    String query = "UPDATE architects set latitude = ?, longitude = ? WHERE arch_id = ?;";
                    PreparedStatement ps = connection.prepareStatement(query);
                     
                    ps.setDouble(1, map.getLat());
                    ps.setDouble(2, map.getLng());
                    ps.setString(3, map.getIds().substring(1));
                   
                    flag = ps.executeUpdate();
                    ps.close();
               
                   
                    connection.close();
                   
                }
                else
                {
                    Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "");
                   
                    String query = "UPDATE architectural_works set latitude = ?, longitude = ? WHERE work_id = ?;";
                    PreparedStatement ps = connection.prepareStatement(query);
                   
                    ps.setDouble(1, map.getLat());
                    ps.setDouble(2, map.getLng());
                    ps.setInt(3, Integer.parseInt(map.getIds().substring(1)));
                   
                    flag = ps.executeUpdate();
                    ps.close();
               
                   
                    connection.close();
                }
        }catch (Exception e) {
                System.out.println(e.getMessage());
                flag = 0;
        }
       
        return flag;
    }

    public ArrayList<ArchitectDTO> filterArchitectsTable(ArrayList<ArchitectDTO> architects, String param, String value1, String value2){
        ArrayList<ArchitectDTO> result = new ArrayList<ArchitectDTO>();
        if(param.equals("epoch") || param.equals("protected") || param.equals("purpose") || param.equals("world_wonder") || param.equals("year_built")) {
            if(param.equals("epoch")) {
                for(ArchitectDTO a : architects) {
                    ArchitectDTO b = a;
                    ArrayList<ArchitecturalWorkDTO> dtos = new ArrayList<ArchitecturalWorkDTO>();
                    for(ArchitecturalWorkDTO dto : a.getWorks()) {
                        if(dto.getEpoch().equals(value1)) {
                            dtos.add(dto);
                        }
                    }

                    if(dtos.size() != 0) {
                   	 b.setWorks(dtos);
                        result.add(b);
                   }
                }
                return result;
            }else if(param.equals("protected")) {
                for(ArchitectDTO a : architects) {
                    ArchitectDTO b = a;
                    ArrayList<ArchitecturalWorkDTO> dtos = new ArrayList<ArchitecturalWorkDTO>();
                    for(ArchitecturalWorkDTO dto : a.getWorks()) {
                        if(dto.isProtectedWork() == true && value1.equals("true")) {
                            dtos.add(dto);
                        }else if(dto.isProtectedWork() == false && value1.equals("false")) {
                            dtos.add(dto);
                        }
                    }
                    if(dtos.size() != 0) {
                    	 b.setWorks(dtos);
                         result.add(b);
                    }
                    
                }
                return result;
            }else if(param.equals("purpose")) {
                for(ArchitectDTO a : architects) {
                    ArchitectDTO b = a;
                    ArrayList<ArchitecturalWorkDTO> dtos = new ArrayList<ArchitecturalWorkDTO>();
                    for(ArchitecturalWorkDTO dto : a.getWorks()) {
                        if(dto.getPurpose().equals(value1)) {
                            dtos.add(dto);
                        }
                    }
                    
                    if(dtos.size() != 0) {
                   	 b.setWorks(dtos);
                        result.add(b);
                   }
                }
                return result;
            }else if(param.equals("world_wonder")) {
                for(ArchitectDTO a : architects) {
                    ArchitectDTO b = a;
                    ArrayList<ArchitecturalWorkDTO> dtos = new ArrayList<ArchitecturalWorkDTO>();
                    for(ArchitecturalWorkDTO dto : a.getWorks()) {
                        if(dto.isWorldWonder() == true && value1.equals("true")) {
                            dtos.add(dto);
                        }else if(dto.isWorldWonder() == false && value1.equals("false")) {
                            dtos.add(dto);
                        }
                    }
                    if(dtos.size() != 0) {
                   	 b.setWorks(dtos);
                        result.add(b);
                   }
                }
                return result;
            }else {
                    try {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date from = null;
                        Date to = null;
                        String new_from = "";
                        new_from += value1.substring(0,10) + " " + value1.substring(11,19);
                        String new_to = "";
                        new_to += value2.substring(0,10) + " " + value2.substring(11,19);
                        from = formatter.parse(new_from);
                        to = formatter.parse(new_to);
                       
                        for(ArchitectDTO a : architects) {
                            ArchitectDTO b = a;
                            ArrayList<ArchitecturalWorkDTO> dtos = new ArrayList<ArchitecturalWorkDTO>();
                            for(ArchitecturalWorkDTO dto : a.getWorks()) {
                                if(dto.getYearBuilt().before(to) && dto.getYearBuilt().after(from)) {
                                    dtos.add(dto);
                                }
                            }
                            b.setWorks(dtos);
                            result.add(b);
                        }
                        return result;
                       
                    }catch(Exception e) {
                        return architects;
                    }
            }
        }else {
            if(param.equals("birth")) {
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss");
                    Date from = formatter.parse(value1.substring(0,24));
                    Date to = formatter.parse(value2.substring(0,24));
                   
                    for(ArchitectDTO dto : architects) {
                        if(dto.getDateBorn().after(from) && dto.getDateBorn().before(to)) {
                            result.add(dto);
                        }
                    }
                    return result;
                   
                }catch(Exception e) {
                    return architects;
                }
            }else if(param.equals("death")) {
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd yyyy HH:mm:ss");
                    Date from = formatter.parse(value1.substring(0,24));
                    Date to = formatter.parse(value1.substring(0,24));
                    
                    
                    for(ArchitectDTO dto : architects) {
                    	if(dto.getDateDeath() == null) {
                    		continue;
                    	}
                        if(dto.getDateDeath().after(from) && dto.getDateDeath().before(to)) {
                            result.add(dto);
                        }
                    }
                    return result;
                   
                }catch(Exception e) {
                	System.out.println(e.getMessage());
                    return architects;
                }
            }else { //nationality
                for(ArchitectDTO dto : architects) {
                    if(dto.getNationality().equals(value1)) {
                        result.add(dto);
                    }
                }
                return result;
            }
        }
    }
   
    public ArrayList<ArchitectDTO> filterArchitectsTable(FilterDTO filter){
        ArrayList<ArchitectDTO> result = filter.getArchitects();
        for(ParamDTO p : filter.getParams()) {
            if(p.isActive() == true) {
                ArrayList<ArchitectDTO> architects = this.filterArchitectsTable(result, p.getParam(), p.getValue1(), p.getValue2());
                result = this.updateResult(result, architects);
            }
        }
        return result;
    }
   
    public ArrayList<ArchitectDTO> updateResult(ArrayList<ArchitectDTO> result, ArrayList<ArchitectDTO> architects){
        ArrayList<ArchitectDTO> result2 = new ArrayList<ArchitectDTO>();
        for(ArchitectDTO dto : result) {
            for(ArchitectDTO neww : architects) {
                if(dto.getId().equals(neww.getId())) {
                    ArrayList<ArchitecturalWorkDTO> works = new ArrayList<ArchitecturalWorkDTO>();
                    works = this.updateWorks(dto.getWorks(), neww.getWorks());
                    dto.setWorks(works);
                    result2.add(dto);
                    break;
                }
            }
        }
        return result2;
    }
   
    public ArrayList<ArchitecturalWorkDTO> updateWorks(ArrayList<ArchitecturalWorkDTO> first, ArrayList<ArchitecturalWorkDTO> second){
        ArrayList<ArchitecturalWorkDTO> new_list = new ArrayList<ArchitecturalWorkDTO>();
        for(ArchitecturalWorkDTO dto : first) {
            for(ArchitecturalWorkDTO dto2 : second) {
                if(dto.getId() == dto2.getId()) {
                    new_list.add(dto);
                    break;
                }
            }
        }
        return new_list;
    }

    public Maps getMap(Integer mapId) {
        return mr.findById(mapId).orElse(null);
    }
 
    public ArrayList<ArchitectDTO> searchArchitects(String param, String type){
        ArrayList<ArchitectDTO> dtos_archs = new ArrayList<ArchitectDTO>();
        String[] chars = param.split("\\ ");
        if(type.equals("m")) {
            if(chars.length == 1) {
                List<Architect> architects = ar.findAll();
                for(Architect a : architects) {
                    if(a.getName().toLowerCase().contains(param.toLowerCase()) || a.getSurname().toLowerCase().contains(param.toLowerCase())) {
                        ArchitectDTO dto = new ArchitectDTO();
                        dto.setName(a.getName());
                        dto.setSurname(a.getSurname());
                        dto.setDateBorn(a.getDateBorn());
                        dto.setDateDeath(a.getDateDeath());
                        dto.setId(a.getId());
                        dto.setLatitude(a.getLatitude());
                        dto.setLongitude(a.getLongitude());
                        dto.setMap_id(a.getMap().getId());
                        dto.setNationality(a.getNationality());
                        dto.setPhoto(a.getPhoto());
                        dto.setWorks(this.getDtos(a));
                        dto.setWorks(this.findWorks(dto.getWorks(), param));
                        dto.setSearchResult(true);
                        dtos_archs.add(dto);
                    }else {
                        ArchitectDTO dto = new ArchitectDTO();
                        dto.setName(a.getName());
                        dto.setSurname(a.getSurname());
                        dto.setDateBorn(a.getDateBorn());
                        dto.setDateDeath(a.getDateDeath());
                        dto.setId(a.getId());
                        dto.setLatitude(a.getLatitude());
                        dto.setLongitude(a.getLongitude());
                        dto.setMap_id(a.getMap().getId());
                        dto.setNationality(a.getNationality());
                        dto.setPhoto(a.getPhoto());
                        dto.setWorks(this.getDtos(a));
                        dto.setWorks(this.findWorks(dto.getWorks(), param));
                        dto.setSearchResult(false);
                        dtos_archs.add(dto);
                    }
                }
                return dtos_archs;
            }else {
                List<Architect> architects = ar.findAll();
                for(Architect a : architects) {
                    if((a.getName().toLowerCase()+" "+a.getSurname().toLowerCase()).contains(param.toLowerCase())) {
                        ArchitectDTO dto = new ArchitectDTO();
                        dto.setName(a.getName());
                        dto.setSurname(a.getSurname());
                        dto.setDateBorn(a.getDateBorn());
                        dto.setDateDeath(a.getDateDeath());
                        dto.setId(a.getId());
                        dto.setLatitude(a.getLatitude());
                        dto.setLongitude(a.getLongitude());
                        dto.setMap_id(a.getMap().getId());
                        dto.setNationality(a.getNationality());
                        dto.setPhoto(a.getPhoto());
                        dto.setWorks(this.getDtos(a));
                        dto.setWorks(this.findWorks(dto.getWorks(), param));
                        dto.setSearchResult(true);
                        dtos_archs.add(dto);
                    }else {
                        ArchitectDTO dto = new ArchitectDTO();
                        dto.setName(a.getName());
                        dto.setSurname(a.getSurname());
                        dto.setDateBorn(a.getDateBorn());
                        dto.setDateDeath(a.getDateDeath());
                        dto.setId(a.getId());
                        dto.setLatitude(a.getLatitude());
                        dto.setLongitude(a.getLongitude());
                        dto.setMap_id(a.getMap().getId());
                        dto.setNationality(a.getNationality());
                        dto.setPhoto(a.getPhoto());
                        dto.setWorks(this.getDtos(a));
                        dto.setWorks(this.findWorks(dto.getWorks(), param));
                        dto.setSearchResult(false);
                        dtos_archs.add(dto);
                    }
                }
                return dtos_archs;
            }
        }else {
            if(chars.length == 1) {
                List<Architect> architects = ar.findAll();
                for(Architect a : architects) {
                    if(a.getName().toLowerCase().contains(param.toLowerCase()) || a.getSurname().toLowerCase().contains(param.toLowerCase())) {
                        ArchitectDTO dto = new ArchitectDTO();
                        dto.setName(a.getName());
                        dto.setSurname(a.getSurname());
                        dto.setDateBorn(a.getDateBorn());
                        dto.setDateDeath(a.getDateDeath());
                        dto.setId(a.getId());
                        dto.setLatitude(a.getLatitude());
                        dto.setLongitude(a.getLongitude());
                        dto.setMap_id(a.getMap().getId());
                        dto.setNationality(a.getNationality());
                        dto.setPhoto(a.getPhoto());
                        dto.setWorks(this.getDtos(a));
                        dto.setSearchResult(true);
                        dtos_archs.add(dto);
                    }else {
                        ArchitectDTO dto = new ArchitectDTO();
                        dto.setName(a.getName());
                        dto.setSurname(a.getSurname());
                        dto.setDateBorn(a.getDateBorn());
                        dto.setDateDeath(a.getDateDeath());
                        dto.setId(a.getId());
                        dto.setLatitude(a.getLatitude());
                        dto.setLongitude(a.getLongitude());
                        dto.setMap_id(a.getMap().getId());
                        dto.setNationality(a.getNationality());
                        dto.setPhoto(a.getPhoto());
                        dto.setWorks(this.getDtos(a));
                        dto.setSearchResult(false);
                        dtos_archs.add(dto);
                    }
                }
                List<ArchitecturalWork> architecturalWorks = awr.findAll();
                for(ArchitecturalWork aw : architecturalWorks) {
                    if(aw.getName().toLowerCase().contains(param.toLowerCase())) {
                        this.addToArchitectsDTO(aw, dtos_archs);
                    }
                }
                return dtos_archs;
            }else {
                List<Architect> architects = ar.findAll();
                for(Architect a : architects) {
                    if((a.getName().toLowerCase()+" "+a.getSurname().toLowerCase()).contains(param.toLowerCase())) {
                        ArchitectDTO dto = new ArchitectDTO();
                        dto.setName(a.getName());
                        dto.setSurname(a.getSurname());
                        dto.setDateBorn(a.getDateBorn());
                        dto.setDateDeath(a.getDateDeath());
                        dto.setId(a.getId());
                        dto.setLatitude(a.getLatitude());
                        dto.setLongitude(a.getLongitude());
                        dto.setMap_id(a.getMap().getId());
                        dto.setNationality(a.getNationality());
                        dto.setPhoto(a.getPhoto());
                        dto.setWorks(this.getDtos(a));
                        dto.setSearchResult(true);
                        dtos_archs.add(dto);
                    }else {
                        ArchitectDTO dto = new ArchitectDTO();
                        dto.setName(a.getName());
                        dto.setSurname(a.getSurname());
                        dto.setDateBorn(a.getDateBorn());
                        dto.setDateDeath(a.getDateDeath());
                        dto.setId(a.getId());
                        dto.setLatitude(a.getLatitude());
                        dto.setLongitude(a.getLongitude());
                        dto.setMap_id(a.getMap().getId());
                        dto.setNationality(a.getNationality());
                        dto.setPhoto(a.getPhoto());
                        dto.setWorks(this.getDtos(a));
                        dto.setSearchResult(false);
                        dtos_archs.add(dto);
                    }
                }
                List<ArchitecturalWork> architecturalWorks = awr.findAll();
                for(ArchitecturalWork aw : architecturalWorks) {
                    if(aw.getName().toLowerCase().contains(param.toLowerCase())) {
                        this.addToArchitectsDTO(aw, dtos_archs);
                    }
                }
                return dtos_archs;
            }
        }
    }
 
    private ArrayList<ArchitecturalWorkDTO> findWorks(ArrayList<ArchitecturalWorkDTO> works, String param) {
        for(ArchitecturalWorkDTO dto : works) {
            if(dto.getName().toLowerCase().contains(param.toLowerCase())) {
                dto.setSearchResult(true);
            }
        }
        return works;
    }
 
    private ArrayList<ArchitecturalWorkDTO> getDtos(Architect a) {
        ArrayList<ArchitecturalWorkDTO> dtos = new ArrayList<ArchitecturalWorkDTO>();
        for(ArchitecturalWork aw : a.getArchitecturalWorks()) {
            ArchitecturalWorkDTO dto = new ArchitecturalWorkDTO();
            dto.setArchitectId(a.getId());
            dto.setDescription(aw.getDescription());
            dto.setEpoch(aw.getEpoch().toString());
            dto.setIcon(aw.getIcon());
            dto.setId(aw.getId());
            dto.setLatitude(aw.getLatitude());
            dto.setLongitude(aw.getLongitude());
            dto.setMapId(aw.getMap().getId());
            dto.setName(aw.getName());
            dto.setProtectedWork(aw.isProtectedWork());
            dto.setPurpose(aw.getPurpose());
            dto.setWorldWonder(aw.isWorldWonder());
            dto.setYearBuilt(aw.getYearBuilt());
            dto.setSearchResult(false);
            dtos.add(dto);
        }
        return dtos;
    }
 
    private void addToArchitectsDTO(ArchitecturalWork aw, ArrayList<ArchitectDTO> dtos_archs) {
        for(ArchitectDTO dto : dtos_archs) {
            if(dto.getId().equals(aw.getArchitect().getId())) {
                int index = this.getIndexOfDTO(aw, dto.getWorks());
                dto.getWorks().get(index).setSearchResult(true);
            }
        }
       
    }
 
    private int getIndexOfDTO(ArchitecturalWork aw, ArrayList<ArchitecturalWorkDTO> works) {
        for(ArchitecturalWorkDTO dto : works) {
            if(dto.getId() == aw.getId()) {
                return works.indexOf(dto);
            }
        }
        return -1;
    }
 
    public ArrayList<ArchitecturalWorkDTO> searchArchitecturalWorks(String param){
        ArrayList<ArchitecturalWorkDTO> result = new ArrayList<ArchitecturalWorkDTO>();
       
        List<ArchitecturalWork> works = awr.findAll();
        for(ArchitecturalWork aw : works) {
            if(aw.getName().toLowerCase().contains(param.toLowerCase())) {
                ArchitecturalWorkDTO dto = new ArchitecturalWorkDTO();
                dto.setArchitectId(aw.getArchitect().getId());
                dto.setDescription(aw.getDescription());
                dto.setEpoch(aw.getEpoch().toString());
                dto.setIcon(aw.getIcon());
                dto.setId(aw.getId());
                dto.setLatitude(aw.getLatitude());
                dto.setLongitude(aw.getLongitude());
                dto.setMapId(aw.getMap().getId());
                dto.setName(aw.getName());
                dto.setProtectedWork(aw.isProtectedWork());
                dto.setPurpose(aw.getPurpose());
                dto.setWorldWonder(aw.isWorldWonder());
                dto.setYearBuilt(aw.getYearBuilt());
                dto.setSearchResult(true);
                result.add(dto);
            }else {
                ArchitecturalWorkDTO dto = new ArchitecturalWorkDTO();
                dto.setArchitectId(aw.getArchitect().getId());
                dto.setDescription(aw.getDescription());
                dto.setEpoch(aw.getEpoch().toString());
                dto.setIcon(aw.getIcon());
                dto.setId(aw.getId());
                dto.setLatitude(aw.getLatitude());
                dto.setLongitude(aw.getLongitude());
                dto.setMapId(aw.getMap().getId());
                dto.setName(aw.getName());
                dto.setProtectedWork(aw.isProtectedWork());
                dto.setPurpose(aw.getPurpose());
                dto.setWorldWonder(aw.isWorldWonder());
                dto.setYearBuilt(aw.getYearBuilt());
                dto.setSearchResult(false);
                result.add(dto);
            }
        }
       
        return result;
       
    }
   
}