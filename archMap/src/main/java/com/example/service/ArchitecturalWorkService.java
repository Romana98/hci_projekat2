package com.example.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dto.ArchitecturalWorkDTO;
import com.example.model.ArchitecturalWork;
import com.example.repository.ArchitecturalWorkRepository;

@Service
public class ArchitecturalWorkService {
	
	@Autowired
	private ArchitecturalWorkRepository awr;

	public int save(@Valid ArchitecturalWork aw) {
		int flag = 0;
		try {
			awr.save(aw);
			flag = awr.findByName(aw.getName()).getId();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			flag = 0;
		}
		return flag;
	}

	public List<ArchitecturalWorkDTO> getArchWorks(int map_id) {
		List<ArchitecturalWorkDTO> works = new ArrayList<ArchitecturalWorkDTO>();
		PreparedStatement st = null;
		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "");
			st = conn.prepareStatement("SELECT * FROM public.architectural_works w LEFT JOIN public.maps m ON w.map = m.map_id where m.map_id = ?;");
			st.setInt(1, map_id);
			
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				ArchitecturalWorkDTO work = new ArchitecturalWorkDTO(rs.getInt("work_id"), rs.getString("name"), rs.getString("description"), new Date(rs.getDate("year_built").getTime()), rs.getString("architect"), rs.getString("epoch"), rs.getBoolean("world_wonder"), rs.getBoolean("protected_work"), rs.getString("purpose"), rs.getString("icon"));
				work.setLatitude(rs.getDouble("latitude"));
				work.setLongitude(rs.getDouble("longitude"));
				work.setSearchResult(false);
				work.setStyle();
				works.add(work);
			}

			rs.close();
			st.close();
			conn.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return works;
		}

		return works;
		
		
	}

	public int edit(ArchitecturalWork aw) {
		int flag = 0;
		try {
			
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "");
			
        	String query = "UPDATE architectural_works set name = ?, description = ?, year_built = ?, architect = ?, epoch = ?, world_wonder = ?, protected_work = ?, purpose = ?, icon = ?, map = ? WHERE work_id = ?;";
 	        PreparedStatement ps = connection.prepareStatement(query);
 	        ps.setString(1, aw.getName());
 			ps.setString(2, aw.getDescription());
 			ps.setTimestamp(3, new Timestamp(aw.getYearBuilt().getTime()));
 			ps.setString(4, aw.getArchitect().getId());
 			ps.setString(5, aw.getEpoch().toString());
 			ps.setBoolean(6, aw.isWorldWonder());
 			ps.setBoolean(7, aw.isProtectedWork());
 			ps.setString(8, aw.getPurpose());
 			ps.setString(9, aw.getIcon());
 			ps.setInt(10, aw.getMap().getId());
 			ps.setInt(11, aw.getId());
 			
 		
 			flag = ps.executeUpdate();
 			ps.close();
       
			
			connection.close();
			
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			return flag;
		}	
			
		return flag;
	}

	public List<ArchitecturalWorkDTO> search(String parameter, String value) {
		List<ArchitecturalWorkDTO> works = new ArrayList<ArchitecturalWorkDTO>();

		PreparedStatement st = null;
		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "");

			if (parameter.equals("Name")) {
				st = conn.prepareStatement("SELECT * FROM architectural_works where name like ?;");
				st.setString(1, "%"+ value+"%");
			
			} 
			else if (parameter.equals("Location")){
				st = conn.prepareStatement("SELECT * FROM architectural_works where location like ?;");
				st.setString(1, "%"+ value+"%");
			
			}
			else if (parameter.equals("Year Built")){
				st = conn.prepareStatement("SELECT * FROM architectural_works where year_built like ?;");
				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(value);
				st.setTimestamp(1, new Timestamp(date.getTime()));
		
			}
			else if (parameter.equals("Architect ID")){
				st = conn.prepareStatement("SELECT * FROM public.architectural_works w LEFT JOIN public.architects a ON w.architect = a.arch_id where a.arch_id like ?;");
				st.setString(1, "%"+ value+"%");
				
			
			}
			else if (parameter.equals("Epoch")){
				st = conn.prepareStatement("SELECT * FROM architectural_works where epoch like ?;");
				st.setString(1, "%"+ value+"%");
			
			}
			else if (parameter.equals("World Wonder")){
				st = conn.prepareStatement("SELECT * FROM architectural_works where world_wonder = ?;");
				st.setBoolean(1, Boolean.parseBoolean(value));
			}
			else {
				st = conn.prepareStatement("SELECT * FROM architectural_works where protected_work = ?;");
				st.setBoolean(1, Boolean.parseBoolean(value));
			}
			
			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				ArchitecturalWorkDTO work = new ArchitecturalWorkDTO(rs.getInt("work_id"), rs.getString("name"), rs.getString("description"), new Date(rs.getDate("year_built").getTime()), rs.getString("architect"), rs.getString("epoch"), rs.getBoolean("world_wonder"), rs.getBoolean("protected_work"), rs.getString("purpose"), rs.getString("icon"));
				works.add(work);
			}

			rs.close();
			st.close();
			conn.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return works;
		}

		return works;
	}

	public int delete(int work_id) {
		int flag = 0;
		try {
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "");
	        
	        String query = "DELETE FROM  architectural_works WHERE work_id = ?";
	        PreparedStatement ps = connection.prepareStatement(query);
			ps.setInt(1, work_id);
			
			flag = ps.executeUpdate();
			
			connection.close();
			ps.close();
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			flag = 0;
		}
		
		return flag;
	}

	public List<ArchitecturalWork> getArchWorks() {
		return awr.findAll();
	}
	
	public List<String> getEpochs(){
		List<ArchitecturalWork> aws = awr.findAll();
		List<String> epochs = new ArrayList<String>();
		for(ArchitecturalWork aw : aws) {
			epochs.add(aw.getEpoch().toString());
		}
		return epochs;
	}
	
	public List<String> getPurposes(){
		List<ArchitecturalWork> aws = awr.findAll();
		List<String> epochs = new ArrayList<String>();
		for(ArchitecturalWork aw : aws) {
			epochs.add(aw.getPurpose());
		}
		return epochs;
	}

}
