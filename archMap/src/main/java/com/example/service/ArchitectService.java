package com.example.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dto.ArchitectDTO;
import com.example.dto.ArchitecturalWorkDTO;
import com.example.model.Architect;
import com.example.model.ArchitecturalWork;
import com.example.model.Maps;
import com.example.repository.ArchitectRepository;

@Service
public class ArchitectService {

	@Autowired
	private ArchitectRepository ar;

	public int addArchitect(@Valid ArchitectDTO adto) {
		try {
			Architect a = new Architect();
			a.setDateBorn(adto.getDateBorn());
			a.setDateDeath(adto.getDateDeath());
			a.setId(adto.getId());
			a.setMap(new Maps(adto.getMap_id()));
			a.setName(adto.getName());
			a.setNationality(adto.getNationality());
			a.setPhoto(adto.getPhoto());
			a.setLongitude(0.0);
			a.setLatitude(0.0);
			a.setSurname(adto.getSurname());

			ar.save(a);
			return 1;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	public Architect getArchitect(String id) {
		return ar.findById(id).orElse(null);
	}

	public List<String> getNationalities() {
		List<String> nationalities = new ArrayList<String>();
		List<Architect> archs = ar.findAll();
		for (Architect arh : archs) {
			if (!nationalities.contains(arh.getNationality())) {
				nationalities.add(arh.getNationality());
			}
		}
		return nationalities;
	}

	public List<ArchitectDTO> getAllArchitectsDTO(int map_id) {
		List<ArchitectDTO> archs = new ArrayList<ArchitectDTO>();
		PreparedStatement st = null;
		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "");
			st = conn.prepareStatement(
					"SELECT * FROM public.architects a LEFT JOIN public.maps m ON a.map = m.map_id where m.map_id = ?;");
			st.setInt(1, map_id);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				ArchitectDTO dto = new ArchitectDTO();
				dto.setDateBorn(new Date(rs.getTimestamp("birth").getTime()));
				if (rs.getTimestamp("death") == null) {
					dto.setDateDeath(null);
				} else {
					dto.setDateDeath(new Date(rs.getTimestamp("death").getTime()));
				}
				dto.setId(rs.getString("arch_id"));
				dto.setName(rs.getString("name"));
				dto.setNationality(rs.getString("nationality"));
				dto.setPhoto(rs.getString("photo"));
				dto.setLatitude(rs.getDouble("latitude"));
				dto.setLongitude(rs.getDouble("longitude"));
				dto.setSurname(rs.getString("surname"));
				dto.setMap_id(map_id);
				dto.setSearchResult(false);
				dto.setStyle();
				archs.add(dto);
			}

			rs.close();
			st.close();
			conn.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return archs;
		}

		return archs;

	}

	public ArrayList<ArchitectDTO> getAllArchitectsDTO() {
		ArrayList<ArchitectDTO> dtos = new ArrayList<ArchitectDTO>();
		ArrayList<Architect> archs = new ArrayList<Architect>();
		archs = (ArrayList<Architect>) ar.findAll();

		for (Architect a : archs) {
			ArchitectDTO dto = new ArchitectDTO();
			dto.setDateBorn(a.getDateBorn());
			dto.setDateDeath(a.getDateDeath());
			dto.setId(a.getId());
			dto.setName(a.getName());
			dto.setNationality(a.getNationality());
			dto.setPhoto(a.getPhoto());
			dto.setLatitude(a.getLatitude());
			dto.setLongitude(a.getLongitude());
			dto.setSurname(a.getSurname());
			dto.setMap_id(a.getMap().getId());
			for (ArchitecturalWork aw : a.getArchitecturalWorks()) {
				ArchitecturalWorkDTO awdto = new ArchitecturalWorkDTO();
				awdto.setId(aw.getId());
				awdto.setName(aw.getName());
				awdto.setLatitude(aw.getLatitude());
				awdto.setLongitude(aw.getLongitude());
				awdto.setDescription(awdto.getDescription());
				awdto.setYearBuilt(aw.getYearBuilt());
				awdto.setArchitectId(a.getId());
				awdto.setEpoch(aw.getEpoch().toString());
				awdto.setWorldWonder(aw.isWorldWonder());
				awdto.setProtectedWork(aw.isProtectedWork());
				awdto.setPurpose(aw.getPurpose());
				dto.addWork(awdto);
			}

			dtos.add(dto);
		}
		return dtos;
	}

	public int editArchitect(@Valid ArchitectDTO adto) {
		int flag = 0;

		try {
			if (adto.getDateDeath() == null) {
				Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
						"postgres", "");

				String query = "UPDATE architects set name = ?, surname = ?, birth = ?, death = ?, nationality = ?, photo = ?, map = ? WHERE arch_id = ?;";
				PreparedStatement ps = connection.prepareStatement(query);
				ps.setString(1, adto.getName());
				ps.setString(2, adto.getSurname());
				ps.setTimestamp(3, new Timestamp(adto.getDateBorn().getTime()));
				ps.setTimestamp(4, null);
				ps.setString(5, adto.getNationality());
				ps.setString(6, adto.getPhoto());
				ps.setInt(7, adto.getMap_id());
				ps.setString(8, adto.getId());

				flag = ps.executeUpdate();
				ps.close();

				connection.close();
			} else {
				Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
						"postgres", "");

				String query = "UPDATE architects set name = ?, surname = ?, birth = ?, death = ?, nationality = ?, photo = ? WHERE arch_id = ?;";
				PreparedStatement ps = connection.prepareStatement(query);
				ps.setString(1, adto.getName());
				ps.setString(2, adto.getSurname());
				ps.setTimestamp(3, new Timestamp(adto.getDateBorn().getTime()));
				ps.setTimestamp(4, new Timestamp(adto.getDateDeath().getTime()));
				ps.setString(5, adto.getNationality());
				ps.setString(6, adto.getPhoto());
				ps.setString(7, adto.getId());

				flag = ps.executeUpdate();
				ps.close();

				connection.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

		return flag;
	}

	public ArrayList<ArchitectDTO> searchArchitect(String parameter, String value) {
		List<ArchitectDTO> dtos = new ArrayList<ArchitectDTO>();

		PreparedStatement st = null;
		Connection conn = null;

		try {
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "");

			if (parameter.equals("Name")) {
				st = conn.prepareStatement("SELECT * FROM architects where name like ?;");
				st.setString(1, "%" + value + "%");

			} else if (parameter.equals("Surname")) {
				st = conn.prepareStatement("SELECT * FROM architects where surname like ?;");
				st.setString(1, "%" + value + "%");

			} else if (parameter.equals("Date born")) {
				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(value);
				st = conn.prepareStatement("SELECT * FROM architects where birth = ?;");
				st.setTimestamp(1, new Timestamp(date.getTime()));

			} else if (parameter.equals("Date death")) {
				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(value);
				st = conn.prepareStatement("SELECT * FROM architects where death = ?;");
				st.setTimestamp(1, new Timestamp(date.getTime()));

			} else {
				st = conn.prepareStatement("SELECT * FROM architects where nationality like ?;");
				st.setString(1, "%" + value + "%");
			}

			ResultSet rs = st.executeQuery();

			while (rs.next()) {
				ArchitectDTO dto = new ArchitectDTO();
				dto.setDateBorn(new Date(rs.getTimestamp("birth").getTime()));
				if (rs.getTimestamp("death") == null) {
					dto.setDateDeath(null);
				} else {
					dto.setDateDeath(new Date(rs.getTimestamp("death").getTime()));
				}
				dto.setId(rs.getString("arch_id"));
				dto.setName(rs.getString("name"));
				dto.setNationality(rs.getString("nationality"));
				dto.setPhoto(rs.getString("photo"));
				dto.setLatitude(rs.getDouble("latitude"));
				dto.setLongitude(rs.getDouble("longitude"));
				dto.setSurname(rs.getString("surname"));
				dtos.add(dto);
			}

			rs.close();
			st.close();
			conn.close();

		} catch (Exception e) {

			return null;
		}

		return (ArrayList<ArchitectDTO>) dtos;
	}

	public int delete(String arch_id) {
		int flag = 0;
		try {
			Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres",
					"");

			String query = "DELETE FROM architects WHERE arch_id = ?";
			PreparedStatement ps = connection.prepareStatement(query);
			ps.setString(1, arch_id);

			flag = ps.executeUpdate();

			connection.close();
			ps.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			flag = 0;
		}

		return flag;
	}
}
