import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpClient, HttpParams} from '@angular/common/http';
import {icon, latLng, LayerGroup, marker, polyline, tileLayer, divIcon, Polyline, Marker} from 'leaflet';


@Component({
  selector: 'app-add-architect',
  templateUrl: './add-architect.component.html',
  styleUrls: ['./add-architect.component.css']
})
export class AddArchitectComponent implements OnInit {

  model: architectModel = {
    id: '',
    name: '',
    surname: '',
    dateBorn: null,
    dateDeath: null,
    latitude: 0,
    longitude: 0,
    nationality: '',
    photo: '',
    map_id: -1
  }

  items: LayerGroup = new LayerGroup();
  options = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 10, attribution: '...' })
    ],
    zoom: 2,
    center: latLng(48, -4)
  };

  maps = [1, 2, 3, 4];
  architects:any[];
  works:any[];

  today: Date;
  years20ago: Date;
  selectedMap:any = 1;

  constructor(private _snackBar: MatSnackBar, private http: HttpClient) { }

  async ngOnInit() {
    this.today = new Date();
    this.years20ago = new Date();
    this.years20ago.setDate(this.today.getDate());
    this.years20ago.setMonth(this.today.getMonth());
    this.years20ago.setFullYear(this.today.getFullYear() - 20);
    let params = new HttpParams();
    params = params.append('map_id', this.selectedMap);
    // @ts-ignore
    this.architects = await this.http.get("http://localhost:8081/architects/getArchitectsMap",{params:params}).toPromise();

    // @ts-ignore
    this.works = await this.http.get("http://localhost:8081/architectural_works/getArchWorksMap",{params:params}).toPromise();
    this.onMapReady();
  }

  onMapReady() {
    let url = "http://localhost:8081/maps/saveMap";
    this.architects.forEach(arch => {
      let routes: Array<Polyline> = [];
      this.works.forEach(work => {
        if (work.architectId === arch.id) {
          let route = polyline([[arch.latitude, arch.longitude],
            [work.latitude, work.longitude]], {color: 'black'}).addTo(this.items);

          routes.push(route);
          if(work.icon.length != 0)
          {
            let mark = marker([work.latitude, work.longitude], {
              draggable: true,
              title: "w" + work.id,
              icon: divIcon({
                iconSize: [60, 60],
                iconAnchor: [ 13, 41 ],
                iconUrl: work.icon,
                html: '<img style="' + work.style + '" src="' + work.icon + '">'
              })
            }).on('dragend', (e) => {
              this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
                res => {
                  mark.setLatLng(e.target.getLatLng());

                  let vals = route.getLatLngs();
                  // @ts-ignore
                  route.setLatLngs([[vals[0].lat, vals[0].lng], [e.target.getLatLng().lat, e.target.getLatLng().lng]])
                },
                err => {
                  console.log(err);
                }

              );
            }).addTo(this.items);
          }
          else
          {
            let mark = marker([work.latitude, work.longitude], {
              draggable: true,
              title: "w" + work.id,
              icon: divIcon({
                iconSize: null,
                iconAnchor: [13, 41],
                iconUrl: work.icon,
                html: '<span style="font-size: 32px;'+ work.style +'">' + work.name + '</span >'
              })
            }).on('dragend', (e) => {
              this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
                res => {
                  mark.setLatLng(e.target.getLatLng());

                  let vals = route.getLatLngs();
                  // @ts-ignore
                  route.setLatLngs([[vals[0].lat, vals[0].lng], [e.target.getLatLng().lat, e.target.getLatLng().lng]])
                },
                err => {
                  console.log(err);
                }

              );
            }).addTo(this.items);

          }


        }
      })
      if (arch.photo.length != 0) {
        let mark = marker([arch.latitude ,arch.longitude], {
          draggable: true,
          title: "a" + arch.id,
          icon: divIcon({
            iconSize: [60, 60],
            iconAnchor: [ 13, 41 ],
            iconUrl: arch.photo,
            html: '<img style="' + arch.style + '" src="' + arch.photo + '">'
          })
        }).on('dragend', (e)=>{

          this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
            res => {
              mark.setLatLng( e.target.getLatLng());
              routes.forEach(route=>{
                  let vals = route.getLatLngs();
                  // @ts-ignore
                  route.setLatLngs([[e.target.getLatLng().lat ,e.target.getLatLng().lng],[vals[1].lat, vals[1].lng]])
                },
                err => {
                  console.log(err);
                }

              );

            })
        }).addTo(this.items);

      } else
      {
        let mark = marker([arch.latitude ,arch.longitude], {
          draggable: true,
          title: "a" + arch.id,
          icon: divIcon({
            iconSize: null,
            iconAnchor: [ 13, 41 ],
            iconUrl: arch.photo,
            html: '<span style="font-size: 32px;'+ arch.style +'">' + arch.name[0] + arch.surname[0]+ '</span >'
          })
        }).on('dragend', (e)=>{

          this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
            res => {
              mark.setLatLng( e.target.getLatLng());
              routes.forEach(route=>{
                  let vals = route.getLatLngs();
                  // @ts-ignore
                  route.setLatLngs([[e.target.getLatLng().lat ,e.target.getLatLng().lng],[vals[1].lat, vals[1].lng]])
                },
                err => {
                  console.log(err);
                }

              );

            })
        }).addTo(this.items);

      }

    });

    this.works.forEach(wo => {
      let flag = 0;
      this.architects.forEach(arch => {
        if (wo.architectId === arch.id) {
          flag = 1;
        }
      });

      if(flag === 0)
      {
        if(wo.icon.length != 0)
        {
          let mark = marker([wo.latitude, wo.longitude], {
            draggable: true, title: "w" + wo.id,
            icon: divIcon({
              iconSize: [60, 60],
              iconAnchor: [ 13, 41 ],
              iconUrl: wo.icon,
              html: '<img style="' + wo.style + '" src="' + wo.icon + '">'
            })
          }).on('dragend', (e) => {
            this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
              res => {
                mark.setLatLng(e.target.getLatLng());

              },
              err => {
                console.log(err);
              }

            );

          }).addTo(this.items);
        }
        else
        {
          let mark = marker([wo.latitude, wo.longitude], {
            draggable: true, title: "w" + wo.id,
            icon: divIcon({
              iconSize: null,
              iconAnchor: [13, 41],
              iconUrl: wo.icon,
              html: '<span style="font-size: 32px;'+ wo.style +'">' + wo.name + '</span >'
            })
          }).on('dragend', (e) => {
            this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
              res => {
                mark.setLatLng(e.target.getLatLng());

              },
              err => {
                console.log(err);
              }

            );

          }).addTo(this.items);
        }
      }
    });
  }

  layerMainGroup: LayerGroup[] = [
    this.items
  ];

  async selectChanged(){
    this.items.clearLayers();
    let params = new HttpParams();
    params = params.append('map_id', this.selectedMap);
    // @ts-ignore
    this.architects = await this.http.get("http://localhost:8081/architects/getArchitectsMap",{params:params}).toPromise();

    // @ts-ignore
    this.works = await this.http.get("http://localhost:8081/architectural_works/getArchWorksMap",{params:params}).toPromise();
    this.onMapReady();
  }

  addArchitect(ngForm: any): void {
    this.model.map_id = this.selectedMap;
    let url = "http://localhost:8081/architects/addArchitect"
    let url1 = "http://localhost:8081/maps/saveMap";
    this.http.post(url, this.model).subscribe(
      () => {
        this._snackBar.open("Architect added successfully! Now you can manage its position on the map!", "Close", {
          duration: 2000,
        });

        if (this.model.photo.length != 0) {
          let mark = marker([this.model.latitude ,this.model.longitude], {draggable: true, title:"a"+ this.model.id,
            icon: icon({
              iconSize: [50,50],
              iconAnchor: [ 13, 41 ],
              iconUrl: this.model.photo
            })
          }).on('dragend', (e)=>{
            this.http.post(url1,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
              res => {
                mark.setLatLng(e.target.getLatLng());
              },
              err => {
                console.log(err);
              }

            );
          }).addTo(this.items);

        } else {
          let mark = marker([this.model.latitude, this.model.longitude], {
            draggable: true,
            title: "a"+this.model.id,
            icon: divIcon({
              iconSize: null,
              iconAnchor: [13, 41],
              iconUrl: this.model.photo,
              html: '<span style="font-size: 32px">' + this.model.name[0] + this.model.surname[0] + '</span >'
            })
          }).on('dragend', (e) => {
            this.http.post(url1,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
              res => {
                mark.setLatLng(e.target.getLatLng());
              },
              err => {
                console.log(err);
              }

            );
          }).addTo(this.items);
        }

        ngForm.resetForm();
        this.model.photo="";
      },
      err => {

        this._snackBar.open("Error has occurred while adding architect!", "Close", {
          duration: 2000,
        });
        console.log(err);
      }
    );

  }

  onImg(event)
  {
    event.preventDefault = true;
    const file = event.target.files[0];
    if (!file) {
      return false
    }
    if (!file.type.match('image.*')) {
      return false
    }
    const reader = new FileReader()
    const that = this
    reader.onload = function (e) {
      that.model.photo = <string> e.target.result
    }
    reader.readAsDataURL(file);
  }

  removeInput() {
    this.model.photo = '';
  }

}

export interface architectModel {
  id: string;
  name: string  | RegExp;
  surname: string;
  dateBorn: Date;
  dateDeath: Date;
  longitude: number,
  latitude: number,
  nationality: string  | RegExp;
  photo: string;
  map_id: number;
}
