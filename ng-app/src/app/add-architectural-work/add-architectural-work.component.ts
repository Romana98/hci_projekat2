import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpClient, HttpParams} from '@angular/common/http';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {divIcon, icon, latLng, LayerGroup, Marker, marker, polyline, Polyline, tileLayer} from 'leaflet';

@Component({
  selector: 'app-add-architectural-work',
  templateUrl: './add-architectural-work.component.html',
  styleUrls: ['./add-architectural-work.component.css']
})
export class AddArchitecturalWorkComponent implements OnInit {

  model : architecturalWorkModel = {
    id: -1,
    name: '',
    latitude: 0,
    longitude: 0,
    description: '',
    yearBuilt: null,
    architectId: '',
    epoch: '',
    worldWonder: false,
    protectedWork: false,
    purpose: '',
    icon: '',
    mapId: 0
  }
  today: Date = new Date();
  epochs = ['PREHISTORIC_ARCHITECTURE', 'ANCIENT_MESOPOTAMIA', 'ANTIQUE', 'BYZANTIUM', 'EARLY_CHRISTIANITY',
            'ROMANESQUE', 'GOTHIC', 'RENAISSANCE', 'BAROQUE', 'CLASSICISM', 'MODERNISM', 'POSTMODERNISM'];
  arch_ids: any;

  constructor(private _snackBar: MatSnackBar, private http: HttpClient) { }

  AddArchWork(ngForm: any){
    this.model.mapId = this.selected_map;
    let url = "http://localhost:8081/architectural_works/addArchWork"
    this.http.post(url, this.model).subscribe(
      res => {
        this._snackBar.open("Architectural work added successfully! Now you can manage its position on the map!", "Close", {
          duration: 2000,
        });
        this.items.clearLayers();
        this.ngOnInit()
        ngForm.resetForm();
        this.model.icon="";


      },
      err => {
        if(err.status == 409) {
          this._snackBar.open("Architect ID doesn't exists!", "Close", {
            duration: 2000,
          });
          console.log(err);
        }else{
          this._snackBar.open("Name taken or you item on predefined location!", "Close", {
            duration: 2000,
          });
          console.log(err);
        }

      }
    );

  }

  onImg(event)
  {
    event.preventDefault = true;
    const file = event.target.files[0];
    if (!file) {
      return false
    }
    if (!file.type.match('image.*')) {
      return false
    }
    const reader = new FileReader()
    const that = this
    reader.onload = function (e) {
      that.model.icon = <string> e.target.result
    }
    reader.readAsDataURL(file);
  }

  removeInput() {
    this.model.icon = '';
  }

  wonderChanged($event: MatCheckboxChange) {
    this.model.worldWonder = $event.checked;
  }

  protectedChanged($event: MatCheckboxChange) {
    this.model.protectedWork = $event.checked;
  }


  async ngOnInit() {
    let params = new HttpParams();
    params = params.append('map_id', this.selected_map);
    // @ts-ignore
    this.architects = await this.http.get("http://localhost:8081/architects/getArchitectsMap",{params:params}).toPromise();

    // @ts-ignore
    this.works = await this.http.get("http://localhost:8081/architectural_works/getArchWorksMap",{params:params}).toPromise();

    this.arch_ids = await this.http.get("http://localhost:8081/architects/getAllArchitects", {params:params}).toPromise();
    this.onMapReady();

  }

  items: LayerGroup = new LayerGroup();
  architects:any[];
  works:any[];
  selected_map:any = 1;


  options = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 10, attribution: '...' })
    ],
    zoom: 2,
    center: latLng(48, -4)
  };

  async onItemChange() {
    this.items.clearLayers();
    let params = new HttpParams();
    params = params.append('map_id', this.selected_map);

    // @ts-ignore
    this.architects = await this.http.get("http://localhost:8081/architects/getArchitectsMap", {params:params}).toPromise();

    // @ts-ignore
    this.works = await this.http.get("http://localhost:8081/architectural_works/getArchWorksMap",{params:params}).toPromise();

    this.onMapReady();
  }

  onMapReady() {
    let url = "http://localhost:8081/maps/saveMap";
    this.architects.forEach(arch => {
      let routes: Array<Polyline> = [];
      this.works.forEach(work => {
        if (work.architectId === arch.id) {
          let route = polyline([[arch.latitude, arch.longitude],
            [work.latitude, work.longitude]], {color: 'black'}).addTo(this.items);

          routes.push(route);
          if(work.icon.length != 0)
          {
            let mark = marker([work.latitude, work.longitude], {
              draggable: true,
              title: "w" + work.id,
              icon: divIcon({
                iconSize: [60, 60],
                iconAnchor: [ 13, 41 ],
                iconUrl: work.icon,
                html: '<img style="' + work.style + '" src="' + work.icon + '">'
              })
            }).on('dragend', (e) => {
              this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
                res => {
                  mark.setLatLng(e.target.getLatLng());

                  let vals = route.getLatLngs();
                  // @ts-ignore
                  route.setLatLngs([[vals[0].lat, vals[0].lng], [e.target.getLatLng().lat, e.target.getLatLng().lng]])
                },
                err => {
                  console.log(err);
                }

              );
            }).addTo(this.items);
          }
          else
          {
            let mark = marker([work.latitude, work.longitude], {
              draggable: true,
              title: "w" + work.id,
              icon: divIcon({
                iconSize: null,
                iconAnchor: [13, 41],
                iconUrl: work.icon,
                html: '<span style="font-size: 32px;'+ work.style +'">' + work.name + '</span >'
              })
            }).on('dragend', (e) => {
              this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
                res => {
                  mark.setLatLng(e.target.getLatLng());

                  let vals = route.getLatLngs();
                  // @ts-ignore
                  route.setLatLngs([[vals[0].lat, vals[0].lng], [e.target.getLatLng().lat, e.target.getLatLng().lng]])
                },
                err => {
                  console.log(err);
                }

              );
            }).addTo(this.items);

          }


        }
      })
      if (arch.photo.length != 0) {
        let mark = marker([arch.latitude ,arch.longitude], {
          draggable: true,
          title: "a" + arch.id,
          icon: divIcon({
            iconSize: [60, 60],
            iconAnchor: [ 13, 41 ],
            iconUrl: arch.photo,
            html: '<img style="' + arch.style + '" src="' + arch.photo + '">'
          })
        }).on('dragend', (e)=>{

          this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
            res => {
              mark.setLatLng( e.target.getLatLng());
              routes.forEach(route=>{
                  let vals = route.getLatLngs();
                  // @ts-ignore
                  route.setLatLngs([[e.target.getLatLng().lat ,e.target.getLatLng().lng],[vals[1].lat, vals[1].lng]])
                },
                err => {
                  console.log(err);
                }

              );

            })
        }).addTo(this.items);

      } else
      {
        let mark = marker([arch.latitude ,arch.longitude], {
          draggable: true,
          title: "a" + arch.id,
          icon: divIcon({
            iconSize: null,
            iconAnchor: [ 13, 41 ],
            iconUrl: arch.photo,
            html: '<span style="font-size: 32px;'+ arch.style +'">' + arch.name[0] + arch.surname[0]+ '</span >'
          })
        }).on('dragend', (e)=>{

          this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
            res => {
              mark.setLatLng( e.target.getLatLng());
              routes.forEach(route=>{
                  let vals = route.getLatLngs();
                  // @ts-ignore
                  route.setLatLngs([[e.target.getLatLng().lat ,e.target.getLatLng().lng],[vals[1].lat, vals[1].lng]])
                },
                err => {
                  console.log(err);
                }

              );

            })
        }).addTo(this.items);

      }

    });

    this.works.forEach(wo => {
      let flag = 0;
      this.architects.forEach(arch => {
        if (wo.architectId === arch.id) {
          flag = 1;
        }
      });

      if(flag === 0)
      {
        if(wo.icon.length != 0)
        {
          let mark = marker([wo.latitude, wo.longitude], {
            draggable: true, title: "w" + wo.id,
            icon: divIcon({
              iconSize: [60, 60],
              iconAnchor: [ 13, 41 ],
              iconUrl: wo.icon,
              html: '<img style="' + wo.style + '" src="' + wo.icon + '">'
            })
          }).on('dragend', (e) => {
            this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
              res => {
                mark.setLatLng(e.target.getLatLng());

              },
              err => {
                console.log(err);
              }

            );

          }).addTo(this.items);
        }
        else
        {
          let mark = marker([wo.latitude, wo.longitude], {
            draggable: true, title: "w" + wo.id,
            icon: divIcon({
              iconSize: null,
              iconAnchor: [13, 41],
              iconUrl: wo.icon,
              html: '<span style="font-size: 32px;'+ wo.style +'">' + wo.name + '</span >'
            })
          }).on('dragend', (e) => {
            this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
              res => {
                mark.setLatLng(e.target.getLatLng());

              },
              err => {
                console.log(err);
              }

            );

          }).addTo(this.items);
        }
      }
    });
  }

  layerMainGroup: LayerGroup[] = [
    this.items
  ];



}

export interface architecturalWorkModel {
  id: number;
  name: string  | RegExp;
  latitude: number;
  longitude: number;
  description: string;
  yearBuilt: Date;
  architectId: string;
  epoch: string;
  worldWonder: boolean;
  protectedWork: boolean;
  purpose: string;
  icon: string;
  mapId: number;
}
