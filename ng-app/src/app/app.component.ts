import {AfterViewInit, Component} from '@angular/core';
import {ShortcutInput} from 'ng-keyboard-shortcuts';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements AfterViewInit {
  title = 'archMap';
  shortcuts: ShortcutInput[] = [];

  constructor(private router: Router) {}
  ngAfterViewInit() {
    this.shortcuts.push(
      {
        key: ['shift + a'],
        command: () => this.router.navigate(['architect/addArchitect']),
        preventDefault: true
      },
      {
        key: ['shift + w'],
        preventDefault: true,
        command: () => this.router.navigate(['architecturalWork/addArchitecturalWork'])

      },
      {
        key: ['alt + a'],
        command: () => this.router.navigate(['architect/editArchitect']),
        preventDefault: true
      },
      {
        key: ['alt + w'],
        command: () => this.router.navigate(['architecturalWork/editArchitecturalWork']),
        preventDefault: true
      },
      {
        key: ['shift + v'],
        command: () => this.router.navigate(['tables']),
        preventDefault: true
      },
      {
        key: ['shift + h'],
        command: () => this.router.navigate(['help']),
        preventDefault: true
      },
      {
        key: ['f1'],
        command: () => this.router.navigate(['help']),
        preventDefault: true
      },
      {
        key: ['shift + d'],
        command: () => this.router.navigate(['demo']),
        preventDefault: true
      },
      {
        key: ['shift + b'],
          command: () => this.router.navigate(['']),
        preventDefault: true
      }
    );
  }
}
