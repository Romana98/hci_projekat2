import {Component, OnInit, ViewChild} from '@angular/core';
import {MatCheckboxChange} from '@angular/material/checkbox';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpClient, HttpParams} from '@angular/common/http';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {DatePipe} from '@angular/common';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-view-edit-architectural-work',
  templateUrl: './view-edit-architectural-work.component.html',
  styleUrls: ['./view-edit-architectural-work.component.css']
})
export class ViewEditArchitecturalWorkComponent implements OnInit {

  model : architecturalWorkModel = {
    id: null,
    name: '',
    latitude: 0,
    longitude: 0,
    description: '',
    yearBuilt: null,
    architectId: '',
    epoch: '',
    worldWonder: false,
    protectedWork: false,
    purpose: '',
    icon: '',
    mapId: -1
  }

  searchModel : searchModel = {
    parameter : '',
    value : ''
  }

  timer:any = 0;
  delay:number = 150;
  prevent:boolean = false;


  today: Date;
  selectedRow: number;

  selected_map:any;

  epochs = ['PREHISTORIC_ARCHITECTURE', 'ANCIENT_MESOPOTAMIA', 'ANTIQUE', 'BYZANTIUM', 'EARLY_CHRISTIANITY',
    'ROMANESQUE', 'GOTHIC', 'RENAISSANCE', 'BAROQUE', 'CLASSICISM', 'MODERNISM', 'POSTMODERNISM'];

  displayedColumns: string[] = ['name', 'yearBuilt', 'architectId',
    'epoch', 'worldWonder', 'protectedWork'];

  options = ["Name", "Year Built", "Architect ID", "Epoch", "World Wonder", "Protected Work"];

  dataSource = new MatTableDataSource();

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  constructor(private _snackBar: MatSnackBar, private http: HttpClient, private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.today = new Date();

    this.http.get("http://localhost:8081/architectural_works/getArchWorks")
      .subscribe((res)=>{
       // @ts-ignore
        this.dataSource.data = res;
      });

    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  onImg(event) {
    event.preventDefault = true;
    const file = event.target.files[0];
    if (!file) {
      return false
    }
    if (!file.type.match('image.*')) {
      return false
    }
    const reader = new FileReader()
    const that = this
    reader.onload = function (e) {
      that.model.icon = <string> e.target.result
    }
    reader.readAsDataURL(file);
  }

  removeInput() {
    this.model.icon = '';
  }

  wonderChanged($event: MatCheckboxChange) {
    this.model.worldWonder = $event.checked;
  }

  protectedChanged($event: MatCheckboxChange) {
    this.model.protectedWork = $event.checked;
  }

  EditArchWork(f:any) {
    this.model.mapId = this.selected_map;
    let url = "http://localhost:8081/architectural_works/editArchWork"
    this.http.post(url, this.model).subscribe(
      () => {
        this._snackBar.open("Architectural work successfully edited!", "Close", {
          duration: 2000,
        });
        this.dataSource.data[this.selectedRow] = JSON.parse(JSON.stringify(this.model));
        // @ts-ignore
        this.dataSource.data[this.selectedRow].yearBuilt = this.datePipe.transform(this.model.yearBuilt, 'yyyy-MM-dd');
        this.dataSource._updateChangeSubscription()
        f.resetForm();
        this.model.icon = "";
      },
      err => {
        if(err.status == 409) {
          this._snackBar.open("Architect ID doesn't exists!", "Close", {
            duration: 2000,
          });
          console.log(err);
        }else{
          this._snackBar.open("Error has occurred while editing architectural work! Change not saved!", "Close", {
            duration: 2000,
          });
          console.log(err);
        }

      }
    );


  }

  selectionChanged(row) {
    this.timer = setTimeout(() => {
      if (!this.prevent) {
    this.selectedRow = this.dataSource.filteredData.indexOf(row);
    this.model.id = row.id;
    this.model.name = row.name;
    this.model.description = row.description;
    this.model.yearBuilt = new Date(row.yearBuilt.substr(0,10));
    this.model.architectId = row.architectId;
    this.model.epoch = row.epoch;
    this.model.worldWonder = row.worldWonder;
    this.model.protectedWork = row.protectedWork;
    this.model.purpose = row.purpose;
    this.model.icon = row.icon;
    this.selected_map = row.mapId;
      }
      this.prevent = false;
    }, this.delay);

  }

  search() {
    let url = "http://localhost:8081/architectural_works/searchArchWork";

    let params = new HttpParams();
    params = params.append('parameter', this.searchModel.parameter );
    params = params.append('value', this.searchModel.value);

    this.http.get(url,{params:params}).subscribe(
      res => {

        // @ts-ignore
        this.dataSource.data = res;
        this.dataSource._updateChangeSubscription()
        this.clearSearch();
      },
      err => {
        if(err.status == 409)
        {
          this.clearSearch();
          this.http.get("http://localhost:8081/architectural_works/getArchWorks")
            .subscribe((res)=>{
              // @ts-ignore
              this.dataSource.data = res;
              this.dataSource._updateChangeSubscription()
            });
        }
        else{
          this.clearSearch();
          this._snackBar.open("No match found!", "Close", {
            duration: 2000,
          });
          console.log(err);
        }
        }

    );

  }

  private clearSearch() {
    this.searchModel.value = '';
    this.searchModel.parameter = '';
  }

  deleteRow(row: any, f: NgForm) {
    this.prevent = true;
    clearTimeout(this.timer);

    let url =  "http://localhost:8081/architectural_works/deleteArchWork"
    let params = new HttpParams().set('work_id', row.id)
    this.http.delete(url,{params:params}).subscribe(
      res => {
        let index = this.dataSource.data.indexOf(row);
        this.dataSource.data.splice(index, 1);
        this.dataSource._updateChangeSubscription()
        if(row.id === this.model.id)
        {
          f.resetForm();
          this.model.icon = "";
        }

        this._snackBar.open("Architectural work deleted successfully", "Close", {
          duration: 2000,
        });

      },
      err => {
        this._snackBar.open("Error has occurred while deleting architectural work", "Close", {
          duration: 2000,
        });

        console.log(err)
      }
    );
  }
}

export interface architecturalWorkModel {
  id: number;
  name: string  | RegExp;
  latitude: number,
  longitude: number,
  description: string;
  yearBuilt: Date;
  architectId: string;
  epoch: string;
  worldWonder: boolean;
  protectedWork: boolean;
  purpose: string;
  icon: string;
  mapId: number;
}

export interface searchModel
{
  parameter: string;
  value : string;

}
