import { Component, OnInit,ViewChild, ComponentFactoryResolver } from '@angular/core';
import {animate, state, style, transition, trigger} from '@angular/animations';
import { MatTableDataSource } from '@angular/material/table';
import { HttpClient, HttpParams } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatPaginator } from '@angular/material/paginator';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-tables-architect-architectural-work',
  templateUrl: './tables-architect-architectural-work.component.html',
  styleUrls: ['./tables-architect-architectural-work.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class TablesArchitectArchitecturalWorkComponent implements OnInit {

  columnsToDisplay = ['name','surname','birth','death','nationality'];

  columnsToDisplay2 = ['name','yearBuilt','epoch','worldwonder','protectedwork'];

  dataSource = new MatTableDataSource();

  expandedElement : any;

  searchVal : String;

  nationalities : any[];

  epochs : any[];

  purposes : any[];

  today : Date;

  params : Param[];

  architects : any[];

  filterModel : FilterModel = {
    nationality : '',
    epoch : '',
    purpose : '',
    world_wonder : 'true',
    protected_work : 'true',
    birth_from : new Date(),
    birth_to : null,
    death_from : new Date(),
    death_to : null,
    year_built_from : new Date(),
    year_built_to : null
  }

  nationality_param : Param = {
    active : false,
    param : 'nationality',
    value1 : '',
    value2 : '',
    date_from : null,
    date_to : null
  }

  epoch_param : Param = {
    active : false,
    param : 'epoch',
    value1 : '',
    value2 : '',
    date_from : null,
    date_to : null
  }

  purpose_param : Param = {
    active : false,
    param : 'purpose',
    value1 : '',
    value2 : '',
    date_from : null,
    date_to : null
  }

  world_wonder_param : Param = {
    active : false,
    param : 'world_wonder',
    value1 : '',
    value2 : '',
    date_from : null,
    date_to : null
  }

  protected_work_param : Param = {
    active : false,
    param : 'protected',
    value1 : '',
    value2 : '',
    date_from : null,
    date_to : null
  }

  birth_param : Param = {
    active : false,
    param : 'birth',
    value1 : '',
    value2 : '',
    date_from : new Date(),
    date_to : null
  }

  death_param : Param = {
    active : false,
    param : 'death',
    value1 : '',
    value2 : '',
    date_from : new Date(),
    date_to : null
  }

  year_built_param : Param = {
    active : false,
    param : 'year_built',
    value1 : '',
    value2 : '',
    date_from : new Date(),
    date_to : null
  }

  all_params : any[];

  filterObject : DTO = {
    architects : null,
    params : null
  }

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;


  constructor(private http:HttpClient,private _snackBar:MatSnackBar,public datePipe: DatePipe) { }

  ngOnInit(): void {
    this.today = new Date();
    this.birth_param.date_to = new Date((new Date(this.today)).getTime() + (60*60*24*1000));
    this.death_param.date_to = new Date((new Date(this.today)).getTime() + (60*60*24*1000));
    this.year_built_param.date_to = new Date((new Date(this.today)).getTime() + (60*60*24*1000));
    this.http.get("http://localhost:8081/architects/getArchitectsAndWorks").subscribe(
      res => {
        // @ts-ignore
        this.architects = res;
        // @ts-ignore
        this.dataSource.data = res;
        /*this.dataSource.data.forEach(element =>{
          // @ts-ignore
          element.dateBorn = this.datePipe.transform(element.dateBorn, 'yyyy-MM-dd');
          // @ts-ignore
          if(element.dateDeath !== null){
            // @ts-ignore
            element.dateDeath = this.datePipe.transform(element.dateDeath, 'yyyy-MM-dd');
          }
          else{
            // @ts-ignore
            element.dateDeath = "/"
          }
          // @ts-ignore
          element.works.forEach(element => {
            element.yearBuilt = this.datePipe.transform(element.yearBuilt, 'yyyy-MM-dd');

          });
        })*/
      },
      err => {
      }
    );
    setTimeout(() => this.dataSource.paginator = this.paginator);
    this.http.get("http://localhost:8081/architects/getNationalities").subscribe(
      res => {
        // @ts-ignore
        this.nationalities = res;
        this.filterModel.nationality = this.nationalities[0];
      },
      err => {
        
      }
    );

    this.http.get("http://localhost:8081/architectural_works/getEpochs").subscribe(
      res => {
        // @ts-ignore
        this.epochs = res;
        this.filterModel.epoch = this.epochs[0];
      },
      err => {
        
      }
    );

    this.http.get("http://localhost:8081/architectural_works/getPurposes").subscribe(
      res => {
        // @ts-ignore
        this.purposes = res;
        this.filterModel.purpose = this.purposes[0];
      },
      err => {
        
      }
    );
  }
  goFilter(){
    console.log("datasourec" + this.dataSource.data)
    console.log(this.architects)
    this.params = new Array<Param>();
    this.birth_param.value1 = this.birth_param.date_from.toString()
    this.birth_param.value2 = this.birth_param.date_to.toString();
    this.death_param.value1 = this.death_param.date_from.toString();
    this.death_param.value2 = this.death_param.date_to.toString();
    this.year_built_param.value1 = this.year_built_param.date_from.toString();
    this.year_built_param.value2 = this.year_built_param.date_to.toString();

    this.params.push(this.nationality_param);
    this.params.push(this.epoch_param);
    this.params.push(this.purpose_param);
    this.params.push(this.world_wonder_param);
    this.params.push(this.protected_work_param);
    this.params.push(this.birth_param);
    this.params.push(this.death_param);
    this.params.push(this.year_built_param);

    this.filterObject = {
      params : this.params,
      architects : this.dataSource.data
    }
    console.log(this.filterObject)
    this.http.post("http://localhost:8081/maps/filterArchitectsTable",this.filterObject).subscribe(
      res => {
        // @ts-ignore
        this.dataSource.data = res;
        console.log(res)
        this.dataSource._updateChangeSubscription;
      },
      err => {
        
      }
    );

  }

  filter(value:any, param:String){
    console.log(param + "je");
    if(value.checked === true){
      if(param == "nationality"){
        this.nationality_param.active = true;
        //this.nationality_param.value1 = this.filterModel.nationality;
      }else if(param == "epoch"){
        this.epoch_param.active = true;
        //this.epoch_param.value1 = this.filterModel.epoch;
      }else if(param == "purpose"){
        this.purpose_param.active = true;
        //this.purpose_param.value1 = this.filterModel.purpose;
      }else if(param == "world_wonder"){
        this.world_wonder_param.active = true;
        //this.world_wonder_param.value1 = this.filterModel.world_wonder;
      }else if(param == "protected"){
        this.protected_work_param.active = true;
        //this.protected_work_param.value1 = this.filterModel.protected_work;
      }else if(param == "birth"){
        this.birth_param.active = true;
        //this.birth_param.value1 = this.filterModel.birth_from.toString();
        //this.birth_param.value2 = this.filterModel.birth_to.toString();
      }else if(param == "death"){
        this.death_param.active = true;
       // this.death_param.value1 = this.filterModel.death_from.toString();
        //this.death_param.value2 = this.filterModel.death_to.toString();

      }else if(param == "year_built"){
        this.year_built_param.active = true;
        //this.year_built_param.value1 = this.filterModel.year_built_from.toString();
        //this.year_built_param.value2 = this.filterModel.year_built_from.toString();
      }
    }
    else{
      if(param == "nationality"){
        this.nationality_param.active = false;
        //this.nationality_param.value1 = this.filterModel.nationality;
      }else if(param == "epoch"){
        this.epoch_param.active = false;
        //this.epoch_param.value1 = this.filterModel.epoch;
      }else if(param == "purpose"){
        this.purpose_param.active = false;
        //this.purpose_param.value1 = this.filterModel.purpose;
      }else if(param == "world_wonder"){
        this.world_wonder_param.active = false;
        //this.world_wonder_param.value1 = this.filterModel.world_wonder;
      }else if(param == "protected"){
        this.protected_work_param.active = false;
        //this.protected_work_param.value1 = this.filterModel.protected_work;
      }else if(param == "birth"){
        this.birth_param.active = false;
        //this.birth_param.value1 = this.filterModel.birth_from.toString();
        //this.birth_param.value2 = this.filterModel.birth_to.toString();
      }else if(param == "death"){
        this.death_param.active = false;
       // this.death_param.value1 = this.filterModel.death_from.toString();
        //this.death_param.value2 = this.filterModel.death_to.toString();

      }else if(param == "year_built"){
        this.year_built_param.active = false;

      }
    }
  }

  search(){
    let param = new HttpParams().set('param', this.searchVal.toString());
    this.http.get("http://localhost:8081/maps/searchArchitectsTable",{params:param}).subscribe(
      res => {
        // @ts-ignore
        this.dataSource.data = res;
        console.log(res);
        this.dataSource._updateChangeSubscription;
      },
      err => {
        
      }
    );
  }

}

export interface FilterModel{
  nationality : String,
  epoch : String,
  purpose : String,
  world_wonder : String,
  protected_work : String,
  birth_from : Date,
  birth_to : Date,
  death_from : Date,
  death_to : Date,
  year_built_to : Date,
  year_built_from : Date
}

export interface Param{
  active : boolean;
  param : String;
  value1 : String;
  value2 : String;
  date_from : Date;
  date_to : Date;
}

export interface DTO{
  architects : any[];
  params : Param[];
}