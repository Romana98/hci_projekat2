import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MapsComponent } from './maps/maps.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './navigation/navigation.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatNativeDateModule} from '@angular/material/core';
import { AddArchitectComponent } from './add-architect/add-architect.component';
import {RouterModule, Routes} from '@angular/router';
import { AddArchitecturalWorkComponent } from './add-architectural-work/add-architectural-work.component';
import { ViewEditArchitecturalWorkComponent } from './view-edit-architectural-work/view-edit-architectural-work.component';
import { ViewEditArchitectComponent } from './view-edit-architect/view-edit-architect.component';
import { TablesArchitectArchitecturalWorkComponent } from './tables-architect-architectural-work/tables-architect-architectural-work.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {HttpClientModule} from '@angular/common/http';
import {KeyboardShortcutsModule} from 'ng-keyboard-shortcuts';
import {MatDividerModule} from '@angular/material/divider';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule} from '@angular/forms';
import {MatOptionModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {DatePipe} from '@angular/common';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import {MatRadioModule} from '@angular/material/radio';
import {MatTabsModule} from '@angular/material/tabs';
import { HelpComponent } from './help/help.component';
import { DemoComponent } from './demo/demo.component';


const appRoutes: Routes = [
  {
    path : '',
    component : MapsComponent
  },
  { path: 'architect',
    children: [
      {
        path : 'addArchitect',
        component : AddArchitectComponent
      },
      {
        path : 'editArchitect',
        component : ViewEditArchitectComponent
      }]
  },
  { path: 'architecturalWork',
    children: [
      {
        path : 'addArchitecturalWork',
        component : AddArchitecturalWorkComponent
      },
      {
        path : 'editArchitecturalWork',
        component : ViewEditArchitecturalWorkComponent
      }]
  },

  {
    path: 'architect',
    children: [
      {
        path : 'addArchitect',
        component : AddArchitectComponent
      },
      {
        path : 'editArchitect',
        component : ViewEditArchitectComponent
      }
    ]
  },

  {
    path : 'tables',
    component : TablesArchitectArchitecturalWorkComponent
  },

  {
    path : 'help',
    component : HelpComponent
  },

  {
    path : 'demo',
    component : DemoComponent
  },

  {
    path : '**',
    component : NavigationComponent
  }
  ];

@NgModule({
  declarations: [
    AppComponent,
    MapsComponent,
    NavigationComponent,
    AddArchitectComponent,
    AddArchitecturalWorkComponent,
    ViewEditArchitecturalWorkComponent,
    ViewEditArchitectComponent,
    TablesArchitectArchitecturalWorkComponent,
    HelpComponent,
    DemoComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    KeyboardShortcutsModule.forRoot(),
    MatToolbarModule,
    RouterModule.forRoot(appRoutes, {
      scrollPositionRestoration: 'enabled', // or 'top'
      anchorScrolling: 'enabled',
      scrollOffset: [0, 64] // [x, y] - adjust scroll offset
    }),
    MatMenuModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    MatFormFieldModule,
    FormsModule,
    MatOptionModule,
    MatSelectModule,
    MatInputModule,
    MatDatepickerModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatNativeDateModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    LeafletModule,
    MatRadioModule,
    MatTabsModule


  ],
  exports: [ RouterModule ],
  providers: [MatDatepickerModule, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
