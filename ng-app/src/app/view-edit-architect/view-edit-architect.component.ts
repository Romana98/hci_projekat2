import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HttpClient, HttpParams} from '@angular/common/http';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {DatePipe} from '@angular/common';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-view-edit-architect',
  templateUrl: './view-edit-architect.component.html',
  styleUrls: ['./view-edit-architect.component.css']
})
export class ViewEditArchitectComponent implements OnInit {

  model: architectModel = {
    id: null,
    name: '',
    surname: '',
    dateBorn: null,
    dateDeath: null,
    latitude: 0,
    longitude: 0,
    nationality: '',
    photo: '',
    map_id: -1
  }

  searchModel : searchModel = {
    parameter : '',
    value : ''
  }

  today: Date;
  ago20years: Date;
  selectedRow: number;

  timer:any = 0;
  delay:number = 150;
  prevent:boolean = false;
  selected_map:any;

  displayedColumns: string[] = ['name', 'surname', 'dateBorn', 'dateDeath', 'nationality'];

  options = ["Name", "Surname", "Date born", "Date death", "Nationality"];


  dataSource = new MatTableDataSource();

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private _snackBar: MatSnackBar, private http: HttpClient, private datePipe: DatePipe) { }

  ngOnInit(): void {

    this.today = new Date();
    this.ago20years = new Date();
    this.ago20years.setDate(this.today.getDate());
    this.ago20years.setMonth(this.today.getMonth());
    this.ago20years.setFullYear(this.today.getFullYear() - 20);
    this.http.get("http://localhost:8081/architects/getArchitects")
      .subscribe((res)=>{
        // @ts-ignore
        this.dataSource.data = res;
        this.dataSource.data.forEach(element =>{
          // @ts-ignore
          if(element.dateDeath !== null){
            // @ts-ignore
            element.dateDeath = this.datePipe.transform(element.dateDeath, 'yyyy-MM-dd');
          }
          else{
            // @ts-ignore
            element.dateDeath = '/';
          }
        })
      });

    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;

  }

  onImg(event)
  {
    event.preventDefault = true;
    const file = event.target.files[0];
    if (!file) {
      return false
    }
    if (!file.type.match('image.*')) {
      return false
    }
    const reader = new FileReader()
    const that = this
    reader.onload = function (e) {
      that.model.photo = <string> e.target.result
    }
    reader.readAsDataURL(file);
  }

  removeInput() {
    this.model.photo = '';
  }

  editArchitect() : void{
    this.model.map_id = this.selected_map;
    let url = "http://localhost:8081/architects/editArchitect";
    this.http.post(url, this.model).subscribe(
      () => {
        this._snackBar.open("Architect successfully edited!", "Close", {
          duration: 2000,
        });
        this.dataSource.data[this.selectedRow] = JSON.parse(JSON.stringify(this.model));
        // @ts-ignore
        this.dataSource.data[this.selectedRow].dateBorn = this.datePipe.transform(this.model.dateBorn, 'yyyy-MM-dd');
        // @ts-ignore
        if(this.dataSource.data[this.selectedRow].dateDeath !== null){
          // @ts-ignore
          this.dataSource.data[this.selectedRow].dateDeath = this.datePipe.transform(this.model.dateDeath, 'yyyy-MM-dd');
        }
        this.dataSource._updateChangeSubscription()
        this.clearForm();
      },
      err => {
        this._snackBar.open("Error has occurred while editing architect! Change not saved!", "Close", {
          duration: 2000,
        });
      }
    );

  }

  selectionChanged(row) {
    this.timer = setTimeout(() => {
      if (!this.prevent) {
    this.selectedRow = this.dataSource.filteredData.indexOf(row);
    this.model.id = row.id;
    this.model.name = row.name;
    this.model.surname = row.surname;
    this.model.dateBorn = new Date(row.dateBorn.substr(0, 10));
    if(row.dateDeath === null){
      this.model.dateDeath = null;
    }else{
      this.model.dateDeath = new Date(row.dateDeath.substr(0,10));
    }
    this.model.nationality = row.nationality;
    this.model.photo = row.photo;
    this.selected_map = row.map_id;
      }
      this.prevent = false;
    }, this.delay);
  }

  clearForm()
  {
    this.model.id = null;
    this.model.name = '';
    this.model.surname = '';
    this.model.dateBorn = null;
    this.model.dateDeath = null;
    this.model.latitude = 0;
    this.model.longitude = 0;
    this.model.nationality = '';
    this.model.photo = '';
  }

  search() : void{
    let url = "http://localhost:8081/architects/searchArchitect";

    let params = new HttpParams();

    params = params.append('parameter', this.searchModel.parameter );
    params = params.append('value', this.searchModel.value);

    this.http.get(url,{params:params}).subscribe(
      res => {

        // @ts-ignore
        this.dataSource.data = res;
        this.dataSource._updateChangeSubscription()
        this.clearSearch();
      },
      err => {
        if(err.status === 409)
        {
          this.clearSearch();
          this._snackBar.open("Invalid date parameter!", "Close", {
            duration: 2000,
          });
        }
        else if(err.status === 400){
          this.clearSearch();
          this._snackBar.open("Parameter input is empty!", "Close", {
            duration: 2000,
          });
        }
        else if(err.status === 417){
          this.clearSearch();
          this._snackBar.open("No matches found!", "Close", {
            duration: 2000,
          });
        }
        else{
          this.clearSearch();
          this._snackBar.open("Something went wrong!", "Close", {
            duration: 2000,
          });
        }
      }

    );
  }

  private clearSearch() {
    this.searchModel.value = '';
    this.searchModel.parameter = '';
  }

  deleteRow(row: any, f: NgForm) {
    this.prevent = true;
    clearTimeout(this.timer);

    let url =  "http://localhost:8081/architects/deleteArchitect";
    let params = new HttpParams().set('arch_id', row.id.toString());
    this.http.delete(url,{params:params}).subscribe(
      res => {
        let index = this.dataSource.data.indexOf(row);
        this.dataSource.data.splice(index, 1);
        this.dataSource._updateChangeSubscription()
        this._snackBar.open("Architect deleted successfully", "Close", {
          duration: 2000,
        });

        if(row.id === this.model.id)
        {
          f.resetForm();
          this.model.photo = "";
        }

      },
      err => {
        if(err.status === 417){
          this._snackBar.open("Error has occurred while deleting architect's works!", "Close", {
            duration: 2000,
          });
        }else if(err.status === 400){
          this._snackBar.open("Error has occurred while deleting architect", "Close", {
            duration: 2000,
          });
        }else{
          this._snackBar.open("Error has occurred while deleting architect", "Close", {
            duration: 2000,
          });
        }
      }
    );
  }

}

export interface architectModel {
  id: string;
  name: string  | RegExp;
  surname: string |RegExp;
  dateBorn: Date;
  dateDeath: Date;
  latitude: number,
  longitude: number,
  nationality: string | RegExp;
  photo: string;
  map_id: number;
}

export interface searchModel
{
  parameter: string;
  value : string;

}
