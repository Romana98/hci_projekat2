import {Component, OnInit} from '@angular/core';
  import {icon, latLng, LayerGroup, marker, polyline, tileLayer, divIcon, Polyline, Marker} from 'leaflet';
import {HttpClient, HttpParams} from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})
export class MapsComponent implements OnInit{

  searchVal : String;

  selected_map:any;
  items: LayerGroup = new LayerGroup();
  options = {
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 10, attribution: '...' })
    ],
    zoom: 2,
    center: latLng(48, -4)
  };

  constructor(private _snackBar: MatSnackBar, private http: HttpClient) {};

  architects:any[];
  works:any[];

  async ngOnInit() {
    this.selected_map = 1;
    let params = new HttpParams();
    params = params.append('map_id', this.selected_map);
    // @ts-ignore
    this.architects = await this.http.get("http://localhost:8081/architects/getArchitectsMap",{params:params}).toPromise();

    // @ts-ignore
    this.works = await this.http.get("http://localhost:8081/architectural_works/getArchWorksMap",{params:params}).toPromise();
    this.onMapReady();

  }


  onMapReady() {
    let url = "http://localhost:8081/maps/saveMap";
    this.architects.forEach(arch => {
      let routes: Array<Polyline> = [];
      this.works.forEach(work => {
        if (work.architectId === arch.id) {
          let route = polyline([[arch.latitude, arch.longitude],
            [work.latitude, work.longitude]], {color: 'black'}).addTo(this.items);

          routes.push(route);
          if(work.icon.length != 0)
          {
            let mark = marker([work.latitude, work.longitude], {
              draggable: true,
              title: "w" + work.id,
              icon: divIcon({
                  iconSize: [60, 60],
                  iconAnchor: [ 13, 41 ],
                  iconUrl: work.icon,
                  html: '<img style="' + work.style + '" src="' + work.icon + '">'
              })
            }).on('dragend', (e) => {
              this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
                res => {
                  mark.setLatLng(e.target.getLatLng());

                  let vals = route.getLatLngs();
                  // @ts-ignore
                  route.setLatLngs([[vals[0].lat, vals[0].lng], [e.target.getLatLng().lat, e.target.getLatLng().lng]])
                },
                err => {
                  console.log(err);
                }

              );
            }).addTo(this.items);
          }
          else
          {
            let mark = marker([work.latitude, work.longitude], {
              draggable: true,
              title: "w" + work.id,
              icon: divIcon({
                iconSize: null,
                iconAnchor: [13, 41],
                iconUrl: work.icon,
                html: '<span style="font-size: 32px;'+ work.style +'">' + work.name + '</span >'
              })
            }).on('dragend', (e) => {
              this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
                res => {
                  mark.setLatLng(e.target.getLatLng());

                  let vals = route.getLatLngs();
                  // @ts-ignore
                  route.setLatLngs([[vals[0].lat, vals[0].lng], [e.target.getLatLng().lat, e.target.getLatLng().lng]])
                },
                err => {
                  console.log(err);
                }

              );
            }).addTo(this.items);

          }


        }
      })
      if (arch.photo.length != 0) {
        let mark = marker([arch.latitude ,arch.longitude], {
          draggable: true,
          title: "a" + arch.id,
          icon: divIcon({
            iconSize: [60, 60],
            iconAnchor: [ 13, 41 ],
            iconUrl: arch.photo,
            html: '<img style="' + arch.style + '" src="' + arch.photo + '">'
          })
        }).on('dragend', (e)=>{

          this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
            res => {
              mark.setLatLng( e.target.getLatLng());
              routes.forEach(route=>{
                  let vals = route.getLatLngs();
                  // @ts-ignore
                  route.setLatLngs([[e.target.getLatLng().lat ,e.target.getLatLng().lng],[vals[1].lat, vals[1].lng]])
            },
            err => {
              console.log(err);
            }

          );

          })
        }).addTo(this.items);

      } else
      {
        let mark = marker([arch.latitude ,arch.longitude], {
          draggable: true,
          title: "a" + arch.id,
          icon: divIcon({
            iconSize: null,
            iconAnchor: [ 13, 41 ],
            iconUrl: arch.photo,
            html: '<span style="font-size: 32px;'+ arch.style +'">' + arch.name[0] + arch.surname[0]+ '</span >'
          })
        }).on('dragend', (e)=>{

          this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
            res => {
              mark.setLatLng( e.target.getLatLng());
              routes.forEach(route=>{
                  let vals = route.getLatLngs();
                  // @ts-ignore
                  route.setLatLngs([[e.target.getLatLng().lat ,e.target.getLatLng().lng],[vals[1].lat, vals[1].lng]])
                },
                err => {
                  console.log(err);
                }

              );

            })
        }).addTo(this.items);

      }

    });

    this.works.forEach(wo => {
      let flag = 0;
      this.architects.forEach(arch => {
        if (wo.architectId === arch.id) {
          flag = 1;
        }
      });

      if(flag === 0)
      {
        if(wo.icon.length != 0)
        {
          let mark = marker([wo.latitude, wo.longitude], {
            draggable: true, title: "w" + wo.id,
            icon: divIcon({
              iconSize: [60, 60],
              iconAnchor: [ 13, 41 ],
              iconUrl: wo.icon,
              html: '<img style="' + wo.style + '" src="' + wo.icon + '">'
            })
          }).on('dragend', (e) => {
            this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
              res => {
                mark.setLatLng(e.target.getLatLng());

              },
              err => {
                console.log(err);
              }

            );

          }).addTo(this.items);
        }
        else
        {
          let mark = marker([wo.latitude, wo.longitude], {
            draggable: true, title: "w" + wo.id,
            icon: divIcon({
              iconSize: null,
              iconAnchor: [13, 41],
              iconUrl: wo.icon,
              html: '<span style="font-size: 32px;'+ wo.style +'">' + wo.name + '</span >'
            })
          }).on('dragend', (e) => {
            this.http.post(url,{ids:mark.getElement().title, lat:e.target.getLatLng().lat,lng:e.target.getLatLng().lng}).subscribe(
              res => {
                mark.setLatLng(e.target.getLatLng());

              },
              err => {
                console.log(err);
              }

            );

          }).addTo(this.items);
        }
      }
    });
  }

  layerMainGroup: LayerGroup[] = [
    this.items
  ];


  async onItemChange() {
    this.items.clearLayers();
    let params = new HttpParams();
    params = params.append('map_id', this.selected_map);

    // @ts-ignore
    this.architects = await this.http.get("http://localhost:8081/architects/getArchitectsMap", {params:params}).toPromise();

    // @ts-ignore
    this.works = await this.http.get("http://localhost:8081/architectural_works/getArchWorksMap",{params:params}).toPromise();
    this.onMapReady();
  }
}
export  interface mapInfo {
  ids: String
  lat: number,
  lng: number
}
